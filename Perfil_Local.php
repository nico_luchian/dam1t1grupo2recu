<?php
session_start();
require_once './bbdd_perfiles.php';

if (isset($_SESSION["userlogin"])) {
    $userlogin_local = $_SESSION["userlogin"];
    ?>
    <!DOCTYPE html>
    <?php
    if (isset($_POST["Asignar"])) {
        $id_concierto = $_POST["idconcierto"];
        $nombre_artistico = $_POST["nomartistico"];
        echo Update_musico_Asignado($nombre_artistico, $id_concierto);
    }
    if (isset($_POST["cancelarConciertobutton"])) {
        $id_concierto = $_POST["idconcierto"];
        echo cancelarConcierto($id_concierto);
    }
    ?>
    <html class="htmlLocales">
        <head>
            <meta charset="UTF-8">
            <title></title>
            <script src="JAVASCRIPT/pluguinJquery.js" type="text/javascript"></script>
            <script src="plugin/dist/jquery.validate.js" type="text/javascript"></script>
            <script src="plugin/dist/additional-methods.js" type="text/javascript"></script>
            <script src="JAVASCRIPT/JS_local_validator.js" type="text/javascript"></script>
            <link href="CSS/CSS_Perfiles.css" rel="stylesheet" type="text/css"/>
            <script src="JAVASCRIPT/jquery.numeric.js" type="text/javascript"></script>
            <script src="JAVASCRIPT/JS_Perfiles.js" type="text/javascript"></script>
        </head>
        <body class="bodyLocales">
            <section class="PrincipalHeader">
                <div class="IntroPagina">
                    <div class="ContenidoHeader">
                        <h1>ZONA<br/> LOCALES</h1>
                        <a class="Boton" href="#Scroll">Entra</a>
                    </div>

                </div>
            </section>



            <div id="Scroll"></div>
            <section class="Body2">
                <div class="ContenidoIzquierda">
                    <div id="Perfil">


                        <?php
                        $nombreUsuLocal = mostrarDatosUsuarioLocal($userlogin_local);
                        while ($fila = mysqli_fetch_array($nombreUsuLocal)) {
                            extract($fila);
                            echo '<img src =' . "usuariosFotos2/" . $imagen . ' class = "Avatar">';
                            echo "<h1>$nombre_local</h1>"
                            . "<label style='color:red'>Ubicación:</label><label id='ubicacion'>$ubicacion</label><br/>"
                            . "<label style='color:red'>Ciudad:</label><label id='municipio'>$municipio</label><br/>"
                            . "<label style='color:red'>Email:</label><label id='email'>$email</label><br/>"
                            . "<label style='color:red'>Telefono:</label><label id='telf'>$telefono</label><br/>"
                            . "<label style='color:red'>Aforo:</label><label id='aforo'>$aforo</><br/>";
                        }
                        ?>
                        <br/>
                        <a id="Boton4" href="LOGOUT.php">Salir</a>
                    </div>
                </div>
                <div class="ContenidoDerecha">
                    <ul>
                        <li id="Modificar"><a>Modificar</a>
                            <ul>

                                <li id="BotUbi"><a>Ubicación</a></li>
                                <li id="BotCiu"><a>Ciudad</a></li>
                                <li id="BotCont"><a>Contacto</a></li>
                                <li id="BotTelf"><a>Telefono</a></li>
                                <li id="BotPerf"><a>Perfil</a></li>
                                <li id="BotPass"><a>Contraseña</a></li>
                            </ul>
                        </li>
                        <li id="BotListConc"><a>Conciertos</a></li>
                        <li id="BotCrearConcierto"><a>Crear</a></li>
                        <li id="ConciertosAsignados"><a>Asignados</a></li>
                    </ul>
                    <div class="DivForm">

                        <form id="changeUbi" class="ModUbi" method="POST">
                            <h1>MODIFICAR UBICACIÓN</h1>
                            <input type="text" name="newUbicacion" placeholder="Ubicación nueva"/><br/>
                            <input type="button" value='modificar ubicación' name="ENVIAR2" id="EnviarUbicacionLocal"/>
                            <div id="respuesta2">Modificado correctamente</div>
                        </form>

                        <form class="ModCiu" method="POST">
                            <h1>MODIFICAR CIUDAD</h1>
                            <?php
                            //desplegable de las ciudades de la bbdd
                            $todo_ciudades = select_ciudades();
                            echo "<select class='DespGenero' name='newCiudad'>";
                            while ($fila = mysqli_fetch_array($todo_ciudades)) {
                                extract($fila);
                                echo
                                "<option value='$municipio'>$municipio</option>";
                            }
                            echo" </select>";
                            ?>
                            <input type="button" value='modificar ciudad' name="ENVIAR3" id="EnviarCiudadLocal"/>
                            <div id="respuesta3">Modificado correctamente</div>
                        </form>

                        <form id="chageEmail" class="ModContact" method="POST">
                            <h1>MODIFICAR CONTACTO</h1>
                            <input type="email" name="newEmail" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}" placeholder="Email nuevo"/><br/>
                            <input type="button" value='modificar email' name="ENVIAR" id="EnviarEmailLocal"/>
                            <div id="respuesta4">Modificado correctamente</div>
                        </form>

                        <form  id="changeTel" class="ModTelf" method="POST" id="modificarTelefonoLocal">
                            <h1>MODIFICAR TELEFONO</h1>
                            <input type="number" class="numeric"  name="newTelefono" id="newTelefono" maxlength="9" placeholder="Telefono nuevo" /><br/>

                            <input type="button" value='modificar telefono' name="ENVIAR" id="EnviarTelefonoLocal"/>
                            <div id="respuesta5">Modificado correctamente</div>

                        </form>
                        <form id="changeLocalData" class="ModPerfil" method="POST">
                            <h1>MODIFICAR PERFIL</h1>

                            <input type="text" name="perfUbicacion" placeholder="Modificar ubicación"/><br/>

                            <?php
                            //desplegable de las ciudades de la bbdd
                            $todo_ciudades = select_ciudades();
                            echo "<select class='DespGenero' name='perfCiudad'>";
                            while ($fila = mysqli_fetch_array($todo_ciudades)) {
                                extract($fila);
                                echo
                                "<option value='$municipio'>$municipio</option>";
                            }
                            echo" </select>";
                            ?>


                            <input type="email" name="perfEmail" placeholder="Modificar email"/><br/>
                            <input type="tel" name="perfTelefono" placeholder="Modificar telefono"/><br/>
                            <input type="text" name="perfAforo" placeholder="Modificar Aforo"/><br/>
                            <input type="submit" name="ENVIAR" id="EnviarPerfilLocal"/>
                            <div id="respuesta6"></div>
                        </form>
                        <form class="ModPass" method="POST">
                            <h1>MODIFICAR CONTRASEÑA</h1>
                            <input type="password" name="oldPass" id="old" placeholder="Contraseña actual"/><br/>
                            <input type="password" name="newPass" id="new" placeholder="Contraseña nueva"/><br/>
                            <input type="password" name="Verificar" id="new2" placeholder="Verificar"/><br/>
                            <input type="submit" name="ENVIAR" id="EnviarPassLocal"/>
                            <div id="respuesta7"></div>
                        </form>
                        <form id="concertValidator" class="CrearConierto" method="POST">
                            <h1>CREAR CONCIERTO</h1>

                            <?php echo"<input type='text' readonly='readonly' style='text-align:center;font-weight:bold;font-size:120%' name='nombrelocal' value='$userlogin_local'/><br/>" ?>
                            <input type="text"  required name="nombreConcierto" placeholder="Nombre del concierto"/><br/>
                            <?php
                            $todo_genero = select_genero_musical();
                            echo "<select class='DespGenero' name='Estilos'>";
                            while ($fila = mysqli_fetch_array($todo_genero)) {
                                extract($fila);
                                echo
                                "<option value='$genero'>$genero</option>";
                            }

                            echo" </select>";
                            ?>
                            <input type="date" name="FechaConcierto" placeholder="Fecha del concierto"/><br/>
                            <input type="time" name="hora" placeholder="Hora del concierto"/><br/>
                            <input type="text" name="Estado" value="0" hidden=""/>
                            <input type="number" name="PrecioEntrada" placeholder="Precio de las entradas"/><br/>
                            <input type="number" name="PropuestaEconomica" placeholder="Propuesta económica"/><br/>
                            <input type="submit" name="ENVIAR" id="BtnCrearConcierto"/>
                            <div id="respuesta8"></div>
                        </form>
                        <div class="ConciertosAsignados">
                            <?php
                            $res = selectConciertosInscritos_por_Local($userlogin_local);
                            echo "<table>";
                            echo "<tr>";
                            echo "<th>Nombre Concierto</th><th>Fecha</th><th>Hora</th><th>Genero</th>"
                            . "<th>Precio entrada</th><th>Musico asignado</th>";
                            echo "</tr>";

                            while ($fila = mysqli_fetch_assoc($res)) {
                                echo "<tr>";
                                extract($fila);
                                echo "<td>$nombre_concierto</td>"
                                . "<td>$fecha_concierto</td>"
                                . "<td>$time_concierto</td>"
                                . "<td>$genero</td>"
                                . "<td>$precio_entrada</td>"
                                . "<td style='background-color:darkorange;color:black;font-weight: bold;'>$nombre_artistico</td>";
                            }
                            echo "</tr>";

                            echo "</table>";
                            ?>
                        </div>


                        <form id='GuardarInfoConcierto' action="Perfil_Local.php" method="GET"></form>

                        <div class="ListaConciertos">
                            <?php
                            $todos_los_conciertos_de_un_local = select_conciertos_creados_por_local($userlogin_local);
                            echo "<table border='0px'>";
                            echo "<tr><th>Nombre concierto</th><th>Fecha</th><th>Hora</><th>genero</th><th>Estado</th><th>Precio entrada</th><th style='color:black;background-color:greenyellow'>Propuesta economica</th><th>Asignación</th></tr>";
                            //echo "<form id='GuardarInfoConcierto>'";

                            while ($fila = mysqli_fetch_array($todos_los_conciertos_de_un_local)) {

                                extract($fila);
                                //echo "<input type='hidden' name='idconcierto' id='idconcierto' value='$id_concierto'/>";
                                echo
                                "<tr><td>$nombre_concierto</td>"
                                . "<td>$fecha_concierto</td>"
                                . "<td>$time_concierto</td>"
                                . "<td>$genero</td>";
                                if ($estado == 2) {
                                    echo "<td><b>Anulado</b></td>";
                                } else if ($estado == 0) {
                                    echo "<td>Vigente</td>";
                                } else {
                                    echo "<td>Asignado</td>";
                                }
                                echo "<td>$precio_entrada</td>"
                                . "<td>$propuesta_economica</td>";

//                                    echo "<td><input type='text' form='GuardarInfoConcierto' name='CONCIERTO_ID' value='$id_concierto'></td>";
                                if ($estado == 2) {
                                    echo "<td><b>Anulado</b></td>";
                                } else if ($estado == 0) {
                                    echo "<td>Vigente</td>";
                                    echo "<td style='background-color:transparent;border:0px'><input type='button' form='GuardarInfoConcierto' name='Musicos' onClick='verMusicos($id_concierto)' value='Ver Musicos' class='ListarMusicos'></td>";
                                    echo "<td style='background-color:transparent;border:0px'><input type='button' form='GuardarInfoConcierto'  name='cancelarConciertobutton' onClick='cancelarConcert($id_concierto)' value='Cancelar' style='background-color:red'></td>";
                                } else {
                                    echo "<td>Asignado</td>";
                                    echo "<td style='background-color:transparent;border:0px'><input type='button' form='GuardarInfoConcierto'  name='cancelarConciertobutton' onClick='cancelarConcert($id_concierto)' value='Cancelar' style='background-color:red'></td>";
                                }



//                                echo "<td><input type='button' ' name='cancelarConciertobutton' onClick='cancelarConcert($id_concierto)' value='Cancelar' style='background-color:red'></td>";
//                                    echo "<td>$nombre_artistico</td>";

                                echo "</tr>";
                                //echo "</form>";
                            }
                            echo " </table>";
                            ?>
           
      
             

                        </div>
                    </div> 
                    <div id="ConciertosXAsignar">

                        <div class="closelistado">
                            <img  src="img/x.png" alt=""/>
                            <form method="POST" id="ListadoMusicosConcierto">
                                <h1>Musicos inscritos al concierto</h1>
                            </form>

                            <div id="conciertosListado">

                            </div>
    <?php
    $listadoMusicosXAsignar = selectMusicoX_idConcierto($id_concierto, $userlogin_local);

                            /*echo "<table border='1px'>";
                            echo "<tr><th style='color:white'>Nombre musico</th><th style='color:white'>Componentes</th><th style='color:white'>Telefono</><th style='color:white'>Web</th><th style='color:white'>Asignación</th></tr>";
                            while ($fila = mysqli_fetch_array($listadoMusicosXAsignar)) {
                                extract($fila);
                                echo
                                "<tr><td>$nombre_artistico</td>"
                                . "<td>$numero_componentes</td>"
                                . "<td>$telefono</td>"
                                . "<td>$web</td>";
                                echo "<input type='hidden' form='ListadoMusicosConcierto' name='idconcierto' id='idconcierto' value='$id_concierto'/>";
                                echo "<input type='hidden' form='ListadoMusicosConcierto' name='nomartistico'  value='$nombre_artistico'/>";

                                echo "<td><input type='submit' form='ListadoMusicosConcierto' name='Asignar' value='Asignar'/></td></tr>";
                            }
                            echo " </table>";*/
    ?>

                        </div>
                    </div>
                </div>
                <div id="Barra">
                    <p>CONTACTO: <br/>
                        c/Sant Quirze, 46-49, 2º 1º A<br/>
                        08005 - Barcelona - España<br/>
                        Tel. +34 93 395 13 78<br/>
                        oohhmusic@oohhmusic.com.es<br/>
                        <a href="mailto:nico_cris_y_alex@gmail.com@gmail.com"> nico_cris_y_alex@gmail.com </a>
                    </p>
                    <img id="Logo" src="logo_nico.png" alt=""/>
                    <div id="Social">
                        <a href="https://es-es.facebook.com/" target="_blank">
                            <img id="Facebook" src="../dam1t1grupo2/img/logos redes sociales/Facebook-PNG-Image-38915.png" alt=""/>
                        </a>
                        <a href="https://www.instagram.com/?hl=es" target="_blank">
                            <img id="Instagram" src="../dam1t1grupo2/img/logos redes sociales/download-instagram-png-logo-20.png" alt=""/>
                        </a>
                        <a href="https://www.youtube.com/" target="_blank">
                            <img id="Youtube" src="../dam1t1grupo2/img/logos redes sociales/f2ea1ded4d037633f687ee389a571086-logotipo-de-youtube-icono-by-vexels.png" alt=""/>
                        </a>
                    </div>

            </section>
        </body>
    </html>


    <?php
} else {
    echo "<p style='color:red; text-align:center; font-size:xx-large'>Tienes que iniciar sesión</p>";
    header("Refresh:2; url=index.php", true, 303);
}
