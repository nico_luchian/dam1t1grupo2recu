<?php



function comprobar_usuario_existe($user) {
    $c = conectar();
    $query = "SELECT * from fan where nombre_usuario_fan = '$user'";
    mysqli_query($c, $query);

    if (mysqli_affected_rows($c) >= 1) {
        return true;
    }

    $query = "SELECT * from locales where nombre_local = '$user'";
    mysqli_query($c, $query);

    if (mysqli_affected_rows($c) >= 1) {
        return true;
    }

    $query = "SELECT * from musico where nombre_artistico = '$user'";
    mysqli_query($c, $query);

    if (mysqli_affected_rows($c) >= 1) {
        return true;
    }
    
    return false;
}

function comprobar_usuario($nombreColumna, $passColumna, $tabla, $usuario, $pass) {

    $c = conectar();
    $query = "SELECT $passColumna from $tabla where $nombreColumna = '$usuario'";
    $result = mysqli_query($c, $query);

    if (mysqli_affected_rows($c) >= 1) {

        $passCifrada = mysqli_fetch_row($result)[0];

        if (password_verify($pass, $passCifrada)) {

            $_SESSION["userlogin"] = $usuario;
            $resultado = true;
        } else {
            $resultado = false;
        }
    } else {

        $resultado = false;
    }

    desconectar($c);
    return $resultado;
}

function bbdd_get_municipio() {
    $c = conectar();
    $select = "SELECT id_ciudad,nombre_ciudad FROM ciudad ORDER BY nombre_ciudad ASC";
    $resultado = mysqli_query($c, $select);
    desconectar($c);
    return $resultado;
}

function bbdd_get_genero() {
    $c = conectar();
    $select = "SELECT genero FROM genero_musical ORDER BY genero ASC";
    $resultado = mysqli_query($c, $select);
    desconectar($c);
    return $resultado;
}

//**************************************************************************INSERTS*********************************************
//INSERTAR FAN
function bbdd_insertar_fan($user, $pass, $nombre, $apellido, $mail, $telefono, $direccion, $id_ciudad, $foto) {
    $c = conectar();
    $insert = "insert into fan values ('$user', '$pass', '$nombre', '$apellido', '$mail','$telefono','$direccion','$id_ciudad','$foto')";
    if (mysqli_query($c, $insert)) {
        $resultado = "ok";
    } else {
        $resultado = mysqli_error($c);
    }
    desconectar($c);
    return $resultado;
}

//INSERTAR MUSICO
function bbdd_insertar_musico($user, $pass, $genero, $nombreComponentes, $nombre, $apellido, $telefono, $id_ciudad, $foto, $mail, $web) {
    $c = conectar();
    $insert = "insert into musico values ('$user', '$pass', '$genero', '$nombreComponentes', '$nombre', '$apellido','$telefono','$id_ciudad','$foto', '$mail', '$web')";
    if (mysqli_query($c, $insert)) {
        $resultado = "ok";
    } else {
        $resultado = mysqli_error($c);
    }
    desconectar($c);
    return $resultado;
}

//INSERTAR LOCAL
function bbdd_insertar_local($nombreLocal, $pass, $ubicacion, $id_ciudad, $mail, $telefono, $aforo, $foto) {
    $c = conectar();
    $insert = "insert into locales values ('$nombreLocal', '$pass', '$ubicacion','$id_ciudad', '$mail', '$telefono', '$aforo', '$foto')";
    if (mysqli_query($c, $insert)) {
        $resultado = "ok";
    } else {
        $resultado = mysqli_error($c);
    }
    desconectar($c);
    return $resultado;
}
if (isset($_POST["action"]) && !empty($_POST["action"])) {
    $action = $_POST["action"];
    onAction($action);
} else {
    $result = Array(
        "resultado" => "Error"
    );
}



function onAction($action) {
    header('content-type: application/json; charset=utf-8');
    ini_set('memory_limit', '16M'); // change 16M to your desired number
    switch ($action) {

        case "Query":
            $query = $_POST["query"];
            $c = conectar();
            mysqli_set_charset($c, "utf8"); //formato de datos utf8
            $result = mysqli_query($c, $query);
            $resultado = mysqli_fetch_all($result, MYSQLI_ASSOC);
            $final = json_encode($resultado);
            echo $final;
            desconectar($c);
            break;
    }
}
//************************************************************************ CONECTION **************************************************
function desconectar($conexion) {
    mysqli_close($conexion);
}
function subirFoto($array) {

    if (isset($array)) {

        $target_dir = "usuariosFotos2/";
        $target_file = $target_dir . basename($array["fileupload"]["name"]);
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

        $check = getimagesize($array["fileupload"]["tmp_name"]);
        if ($check == false) {
            echo "<script>alert('El archivo no es una imagen válida.')</script>";
            $uploadOk = 0;
        }

        // Check if file already exists

        if (file_exists($target_file)) {
//            echo "<script>alert('Error. El archivo de la foto ya existe en el servidor.')</script>";
            $uploadOk = 0;
            return;
        }
        // Check file size
        if ($array["fileupload"]["size"] > 500000) {
            echo "<script>alert('Error. El archivo de la foto es demasiado grande.')</script>";
            $uploadOk = 0;
            return;
        }
        // Allow certain file formats
        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
            echo "<script>alert('Error. Solo se admiten imágenes jpg, png y gif.')</script>";
            $uploadOk = 0;
            return;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "<script>alert('La foto no se ha enviado.')</script>";
            // if everything is ok, try to upload file
            $target_file = '';
        } else {
            if (!move_uploaded_file($array["fileupload"]["tmp_name"], $target_file)) {
                echo "<script>alert('Ha habido un error subiendo la foto.')</script>";
                $target_file = '';
            }
        }
    }
    return $target_file;
}

function conectar() {

    $conexion = mysqli_connect("localhost", "root", "", "proyecto");
//    $conexion = mysqli_connect("localhost", "grupo2", "grupo2", "trasnversalgrupo2");
    if (!$conexion) {
        die("No se ha podido establecer la conexión con el servidor");
    }
    return $conexion;
}
