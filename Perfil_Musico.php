<?php
session_start();
require_once './bbdd_perfiles.php';

if (isset($_SESSION["userlogin"])) {
    $userlogin_musico = $_SESSION["userlogin"];
} else {
    echo "<p style='color:red; text-align:center; font-size:xx-large'>Tienes que iniciar sesión</p>";
    header("Refresh:2; url=index.php", true, 303);
}
?>

<html class="htmlMusico">
    <head>
        <meta charset="UTF-8">
        <title></title>
        <script src="JAVASCRIPT/pluguinJquery.js" type="text/javascript"></script>
        <script src="plugin/dist/jquery.validate.js" type="text/javascript"></script>
        <script src="plugin/dist/additional-methods.js" type="text/javascript"></script>
        <script src="JAVASCRIPT/JS_musico_validator.js" type="text/javascript"></script>
        <link href="CSS/CSS_Perfiles.css" rel="stylesheet" type="text/css"/>
        <script src="JAVASCRIPT/JS_Perfiles.js" type="text/javascript"></script>
        <script src="JAVASCRIPT/jquery.numeric.js" type="text/javascript"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>


    </head>

    <?php
    if (isset($_POST["submit_inscribirse"])) {
        $id_conciertotravez = $_POST["idconcierto"];
        $nombre_artistico = $_SESSION["userlogin"];
        $fecha_inscripcion = date("Y-m-d H:i:s");
        echo inscribirseenConcierto($id_conciertotravez, $nombre_artistico, $fecha_inscripcion);
//        swal("¡OK!", "¡Inscrito al concierto correctamente!", "success");
//        swal('Any fool can use a computer');
    }
    if (isset($_POST["submit_baja"])) {
        $id_concierto = $_POST["idconcierto"];
        $nombre_musico = $_SESSION["userlogin"];
        echo borrarseConcierto($id_concierto, $nombre_musico);
    }
    ?>
    <body class="bodyMusico">
        <section class="PrincipalHeader">
            <div class="IntroPagina">
                <div class="ContenidoHeader">
                    <h1>ZONA<br/> MÚSICOS</h1>
                    <a class="Boton" href="#Scroll">Entra</a>
                </div>

            </div>
        </section>

        <div id="Scroll"></div>
        <section class="Body2">
            <div class="ContenidoIzquierda">
                <div id="Perfil">

                    <!--                    <h1>NEVERMIND</h1>
                                        <label>Nombre: NeverMind</label><br/>
                                        <label>Ubicacion: C/Tallers nº8</label><br/>
                                        <label>Ciudad: Barcelona</label><br/>
                                        <label>Email: nevermind@support.com</label><br/>
                                        <label>Telf: 93560630</label><br/>
                                        <label>Aforo: 180</label><br/>-->
                    <?php
                    $nombreUsuMusico = mostrarDatosUsuarioMusico($userlogin_musico);
                    while ($fila = mysqli_fetch_array($nombreUsuMusico)) {
                        extract($fila);
                        echo '<img src =' . "usuariosFotos2/" . $imagen . ' class = "Avatar">';
                        echo "<h1>$nombre_artistico</h1>"
                        . "<label style='color:gold'>Género:</label><label id='genero'>$genero</label><br/>"
                        . "<label style='color:gold'>Componentes:</label><label id='componentes'>$numero_componentes</label><br/>"
                        . "<label style='color:gold'>Nombre:</label><label id='nombre'>$nombre</label><br/>"
                        . "<label style='color:gold'>Apellidos:</label><label id='apellidos'>$apellidos</label><br/>"
                        . "<label style='color:gold'>Ciudad:</label><label id='municipio'>$municipio</label><br/>"
                        . "<label style='color:gold'>Email:</label><label id='email'>$email</label><br/>"
                        . "<label style='color:gold'>Telefono:</label><label id='telefono'>$telefono</label><br/>"
                        . "<label style='color:gold'>Web:</label><label id='web'>$web</label><br/>";
                    }
                    ?>
                    <br/>
                    <a id="Boton4" href="LOGOUT.php">Salir</a>
                </div>
            </div>
            <div class="ContenidoDerecha">
                <ul>
                    <li><a>Modificar</a>
                        <ul>
                            <li id="BotGen"><a>Género</a></li>
                            <li id="BotComp"><a>Componentes</a></li>
                            <li id="BotNom"><a>Nombre</a></li>
                            <li id="BotApe"><a>Apellidos</a></li>
                            <li id="BotCiu"><a>Ciudad</a></li>
                            <li id="BotCont"><a>Contacto</a></li>
                            <li id="BotTelf"><a>Telefono</a></li>
                            <li id="BotWeb"><a>Web</a></li>
                            <li id="BotPerf"><a>Perfil</a></li>
                            <li id="BotPass"><a>Contraseña</a></li>

                        </ul>
                    </li>
                    <li id="BotListConc"><a>Conciertos</a></li>
                    <li id="ConciertosAsignados"><a>Conciertos inscrito</a></li>
                </ul>
                <div class="DivForm">
                    <form class="ModGen" method="POST">
                        <h1>MODIFICAR GÉNERO</h1>
                        <?php
                        //desplegable de los generos desde la bbdd

                        $todo_genero = select_genero_musical();
                        echo "<select class='DespGenero' name='newGenero'>";
                        while ($fila = mysqli_fetch_array($todo_genero)) {
                            extract($fila);
                            echo
                            "<option value='$genero'>$genero</option>";
                        }

                        echo" </select>";
                        ?>
                        <input type="submit" name="ENVIAR1" id="EnviarGeneroMusico"/>
                        <div id="respuesta">Modificado correctamente</div>
                    </form>
                    <form id="changeComponentesMusico" class="ModComponentes" method="POST">
                        <h1>MODIFICAR COMPONENTES</h1>
                        <input type="number" name="newComponentes" placeholder="Introduce nuevos componentes"/><br/>
                        <input type="button" name="ENVIAR2" value="modificar componentes"id="EnviarComponentesMusico"/>
                        <div id="respuesta2">Modificado correctamente</div>
                    </form>
                    <form id="chageNombremusico" class="ModNom" method="POST">
                        <h1>MODIFICAR NOMBRE</h1>
                        <input type="text" name="newNombre" placeholder="Introduce nombre nuevo"/><br/>
                        <input type="button"  value="modificar nombre"  name="ENVIAR2" id="EnviarNombreMusico"/>
                        <div id="respuesta3">Modificado correctamente</div>
                    </form>
                    <form id="changeApeliidoMusico" class="ModApellido" method="POST">
                        <h1>MODIFICAR APELLIDO</h1>
                        <input type="text" name="newApellido" placeholder="Introduce apellidos nuevos"/><br/>
                        <input type="button"  value="modificar apellidos"  name="ENVIAR3" id="EnviarApellidosMusico"/>
                        <div id="respuesta4">Modificado correctamente</div>
                    </form>
                    <form class="ModCiu" method="POST">
                        <h1>MODIFICAR CIUDAD</h1>


                        <?php
                        //desplegable de las ciudades de la bbdd

                        $todo_ciudades = select_ciudades();
                        echo "<select class='DespGenero' name='newCiudad'>";
                        while ($fila = mysqli_fetch_array($todo_ciudades)) {
                            extract($fila);
                            echo
                            "<option value='$municipio'>$municipio</option>";
                        }

                        echo" </select>";
                        ?>


                        <input type="button"  value="modificar ciudad" name="ENVIAR3" id="EnviarCiudadMusico"/>
                        <div id="respuesta5">Modificado correctamente</div>
                    </form>
                    <form id="chageContactoMusico" class="ModContact" method="POST">
                        <h1>MODIFICAR CONTACTO</h1>
                        <input type="email" name="newEmail" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}" placeholder="Introduce email nuevo"/><br/>
                        <input type="button"  value="modificar email" name="ENVIAR" id="EnviarEmailMusico"/>
                        <div id="respuesta6">Modificado correctamente</div>
                    </form>
                    <form id="changeTelefonoMusico" class="ModTelf" method="POST" id="modificarTelefono">
                        <h1>MODIFICAR TELEFONO</h1>
                        <input type="number" name="newTelefono"  id="newTelefono" maxlength="9" class="numeric"  placeholder="Introduce teléfono nuevo"/><br/>
                        <input type="button"   value="modificar telefono" name="ENVIAR" id="EnviarTelefonoMusico"/>
                        <div id="respuesta10">Modificado correctamente</div>
                    </form>
                    <form id="changeWebMusico" class="ModWeb" method="POST">
                        <h1>MODIFICAR WEB</h1>
                        <input type="text"  name="newWeb" placeholder="Nueva web"/><br/>
                        <input type="button"  value="modificar web"  name="ENVIAR" id="EnviarWebMusico"/>
                        <div id="respuesta9">Modificado correctamente</div>
                    </form>
                    <form id="changeDatosMusico" class="ModPerfil" method="POST">
                        <h1>MODIFICAR PERFIL</h1>
                        <?php
                        $todo_genero = select_genero_musical();
                        echo "<select required class='DespGenero' name='Estilos'>";
                        while ($fila = mysqli_fetch_array($todo_genero)) {
                            extract($fila);
                            echo
                            "<option value='$genero'>$genero</option>";
                        }

                        echo" </select>";
                        ?>
                        <input type="text" required name="perfNombre" placeholder="Modificar nombre"/><br/>
                        <input type="text" required name="perfApellidos" placeholder="Modificar apellidos"/><br/>
                        <input type="number" required name="perfComponentes" placeholder="Modificar componentes"/><br/>
                        <?php
                        //desplegable de las ciudades de la bbdd
                        $todo_ciudades = select_ciudades();
                        echo "<select  required aria-required=true class='DespGenero' name='perfCiudad'>";
                        while ($fila = mysqli_fetch_array($todo_ciudades)) {
                            extract($fila);
                            echo
                            "<option value='$municipio'>$municipio</option>";
                        }
                        echo" </select>";
                        ?>

                        <input type="email"  required aria-required name="perfEmail" placeholder="Modificar email"/><br/>
                        <input type="tel" required name="perfTelefono" placeholder="Modificar telefono"/><br/>
                        <input type="text" required name="perfWeb" placeholder="Modificar web"/><br/>
                        <input type="submit" name="ENVIAR" id="EnviarPerfilMusico"/>

                        <div id="respuesta7">Modificado correctamente</div>
                    </form>

                    </form>
                    <form class="ModPass" method="POST">
                        <h1>MODIFICAR CONTRASEÑA</h1>
                        <input type="password" name="oldPass" placeholder="Contraseña actual"/><br/>
                        <input type="password" name="newPass" placeholder="Contraseña nueva"/><br/>
                        <input type="password" name="Verificar" placeholder="Verificar"/><br/>
                        <input type="button" value="modificar contraseña"  name="ENVIAR" id="EnviarPassMusico"/>
                        <div id="respuesta8">Modificado correctamente</div>
                    </form>


                    <div class="ConciertosAsignados">
                        <h3 style='color:white;text-shadow: 4px 4px 5px black'>Conciertos Inscritos</h3>
                        <?php
                        $res = selectConciertosInscritos($nombre_artistico);
                        echo "<table>";
                        echo "<tr>";
                        echo "<th>Nombre concierto</th><th>Fecha inscripción</th><th>Fecha concierto</th>"
                        . "<th>Respuesta inscripción</th>";
                        echo "</tr>";

                        while ($fila = mysqli_fetch_array($res)) {
                            extract($fila);
                            
//                            print_r($fila);
                        
                            if (stripos($respuesta_inscripcion, 'asignado') !== false) {
                                echo "<tr>";
                                echo "<td>$nombre_concierto</td>"
                                . "<td>$fecha_inscripcion</td>"
                                     . "<td>$fecha_concierto</td>"
                                . "<td style='background-color:green'>$respuesta_inscripcion</td>";
                            } else if (stripos($respuesta_inscripcion, 'rechazado') !== false) {
                                echo "<tr>";
                                echo "<td>$nombre_concierto</td>"
                                . "<td>$fecha_inscripcion</td>"
                                     . "<td>$fecha_concierto</td>"
                                . "<td style='background-color:red'>$respuesta_inscripcion</td>";
                            } else {
                                echo "<tr>";
                                echo "<td>$nombre_concierto</td>"
                                . "<td>$fecha_inscripcion</td>"
                                . "<td>$fecha_concierto</td>"
                                . "<td style='background-color:grey'>$respuesta_inscripcion</td>";
                            }
                        }
                        echo "</tr>";

                        echo "</table>";
                        ?>



                    </div>
                    <div class="ListaConciertos">

                        <h3 style='color:white;text-shadow: 4px 4px 5px black'>Conciertos de tu genero Músical</h3>
                        <?php
                        $todos_conciertos_a_inscribirse = select_conciertos_para_inscribirse($nombre_artistico);

                        echo "<table>";
                        echo "<tr>";
                        echo "<th>Nombre</th><th style='color:black;background-color:greenyellow'>Propuesta económica</th><th>Fecha</th><th>Hora</th>"
                        . "<th>Local</th><th>inscribirse</th>";
                        echo "</tr>";

                        while ($fila = mysqli_fetch_assoc($todos_conciertos_a_inscribirse)) {

                            echo "<tr>";

                            foreach ($fila as $clave => $dato) {
                                if ($clave != "id_concierto" && $clave != "estado") {

                                    echo "<td>$dato</td>";
                                }
                            }
                            echo "<td  style='background-color:transparent;border:0px'>";
                            ?>
                            <form action="Perfil_Musico.php" method=POST>
                                <input type="hidden" name="idconcierto"  value="<?php echo $fila["id_concierto"] ?>">
                                <?php
                                extract($fila);
                                if (inscrito_a_concierto($id_concierto, $nombre_artistico)) {

                                    echo"<input  style='background-color:green'' type='submit' name='submit_inscribirse' value='Inscribirse'>";
                                } else {
                                    echo"<input style='background-color:red' type='submit' name='submit_baja' value='Darse de baja'>";
                                }
                                ?>


                            </form>
                            <?php
                            echo "</td>";

                            //ESTO ES OTRA MANERA DE HACERLO QUE NO FUNCIONA
//                                echo "<td style=border:2px><input type='button' style='background-color:green; color:white'  width='5%' id='inscripcion' value='Inscribete' onClick='inscribete(".$fila["id_concierto"].")'/></td>";

                            echo "</tr>";
                        }
                        echo "</table>";
                        ?>

                       


                    </div>
                </div>    
            </div>
            <div id="Barra">
                <p>CONTACTO: <br/>
                    c/Sant Quirze, 46-49, 2º 1º A<br/>
                    08005 - Barcelona - España<br/>
                    Tel. +34 93 395 13 78<br/>
                    oohhmusic@oohhmusic.com.es<br/>
                    <a href="mailto:nico_cris_y_alex@gmail.com@gmail.com"> nico_cris_y_alex@gmail.com </a>
                </p>
                <img id="Logo" src="img/logo_nico.png" alt=""/>
                <div id="Social">
                    <a href="https://es-es.facebook.com/" target="_blank">
                        <img id="Facebook" src="../dam1t1grupo2/img/logos redes sociales/Facebook-PNG-Image-38915.png" alt=""/>
                    </a>
                    <a href="https://www.instagram.com/?hl=es" target="_blank">
                        <img id="Instagram" src="../dam1t1grupo2/img/logos redes sociales/download-instagram-png-logo-20.png" alt=""/>
                    </a>
                    <a href="https://www.youtube.com/" target="_blank">
                        <img id="Youtube" src="../dam1t1grupo2/img/logos redes sociales/f2ea1ded4d037633f687ee389a571086-logotipo-de-youtube-icono-by-vexels.png" alt=""/>
                    </a>
                </div>
            </div>
        </section>

    </body>
</html>


 <form class="CrearConierto" method="POST" action="Perfil_Musico.php">
                       <?php
                        $todos_conciertos_inscritos = select_de_conciertos_inscritos($nombre_artistico);
                        echo "<table>";
                        echo "<tr>";
                        echo "<th style='color:white;background-color:#990000''>Nombre concierto</th><th style='color:black;background-color:greenyellow'>Propuesta económica</th><th style='color:white;background-color:#990000''>Fecha inscripción</th><th style='color:white;background-color:#990000''>Local</th><th style='color:white;background-color:#990000''>Respuesta inscripción</th>";
                        echo "</tr>";
                        while ($fila = mysqli_fetch_assoc($todos_conciertos_inscritos)) {
                            extract($fila);
                            echo "<tr>";
                            $id_concierto;
                            echo "<td style='color:white'>$nombre_concierto</td>";
                            echo "<td style='color:white'>$nombre_artistico</td>";
                            echo "<td style='color:white'>$fecha_inscripcion</td>";
                            echo "<td style='color:white'>$nombre_local</td>";
                            echo "<td style='color:white'>$respuesta_inscripcion</td>";
                            echo "<td>";
                            
?>
                            <input type="hidden" name="idconcierto2"  value="<?php // echo $id_concierto          ?>">
                            <input type="submit" class="btnInscribir" value="Borrarse" name="Borrarse"/>
                           /<?php
                            echo "</td>";
                            echo "</tr>";
                        }
                        echo "</table>";
?> 
                        <div id="respuesta15">
                        </div>
                    </form>
