


<?php

if (!isset($_SESSION)) {
    /*     * **************CONNEXION****************** */
    session_start();
}

function conectar() {
    $conexion = mysqli_connect("localhost", "root", "", "proyecto");
//        $conexion = mysqli_connect("localhost", "grupo2", "grupo2", "trasnversalgrupo2");
    if (!$conexion) {
        die("No se ha podido establecer la conexión con el servidor");
    }
    return $conexion;
}

function desconectar($conexion) {
    mysqli_close($conexion);
}

if (isset($_POST["action"])) {
//    echo "Action: ". $_POST["action"];
    onAction($_POST["action"]);
}

//function updateNoAceptados($id_concierto) {
//    $c = conectar();
//   
//    $resultado = mysqli_query($c, $select);
//    $result = mysqli_affected_rows($c) >= 1;
//    desconectar($c);
//
//    return $result ? "Borrados" : "Error";
//}

function updateAsignarMusico($nombre_artistico, $id_concierto) {
    $c = conectar();
    $updateRespuesta1 = "update inscribir set respuesta_inscripcion='asignado' where nombre_artistico='$nombre_artistico' and id_concierto='$id_concierto'";
    $updateRespuesta2 = "update inscribir set respuesta_inscripcion='rechazado' where nombre_artistico not like'$nombre_artistico' and id_concierto='$id_concierto'";
    $updateConcierto = "update concierto set nombre_artistico='$nombre_artistico',estado='1' where id_concierto='$id_concierto'";
    $resultado = mysqli_query($c, $updateRespuesta1);
    $resultado2 = mysqli_query($c, $updateRespuesta2);
    $resultado3 = mysqli_query($c, $updateConcierto);
    $result = mysqli_affected_rows($c) >= 1;

    desconectar($c);

    return $result ? "Actualizado" : "Error";
}

//--------------------------------------------------------CANCELAR  CONCIERTO----------------------------------------------------------------------------

function cancelarConcierto($id_concierto) {
    $c = conectar();
    $update = "update concierto set estado='2' where id_concierto='$id_concierto'";
    $delete = "delete from inscribir where id_concierto='$id_concierto'";

    $resultado = mysqli_query($c, $update);
    $resultado = mysqli_query($c, $delete);
     $result = mysqli_affected_rows($c) >= 1;
    desconectar($c);
    return $result;
}

function onAction($action) {
//Dentro de la función onAction lo que hacemos es un switch
//EN EL CASO DE QUE ACTION SEA TAL BOTON HARA UNA COSA O OTRA.
//Con este switch lo que hacemos es AISLAR la recogida de datos por POST
//Y dentro de cada post se llama a la función.
//Hemos tenido muchos problemas a la hora de aislar la recogida de variables
//porque con la función .serialize() del JS cogia todas las variables existentes, y saltaba el error de undefined 
    switch ($action) {

//---------------------------------------------------------------MODIFICAR LOCAL---------------------------------------------------------------------------
        
        case "cancelarConcierto":

            $id_concierto = $_POST["id_concierto"];

            echo cancelarConcierto($id_concierto);

            break;
        case "UpdateAsignarMusico":
            $nombre = $_POST["nombre_artistico"];
            $id_concierto = $_POST["id_concierto"];

            echo updateAsignarMusico($nombre, $id_concierto);

            break;
        case "ModificarUbicacionLocal":
            $nombre = $_SESSION["userlogin"];
            $perfil = "locales";
            $id_de_aquien = "nombre_local";
            $nuevaPropiedad = $_POST["newUbicacion"];
            echo modificarubicacion($perfil, $id_de_aquien, $nuevaPropiedad, $nombre);
            break;

        case "ModificarCiudadLocal";
            $nombre = $_SESSION["userlogin"];
            $perfil = "locales";
            $id_de_aquien = "nombre_local";
            $newCiudad = $_POST["newCiudad"];
            $place = "ubicacion";
            echo modificarciudad($perfil, $id_de_aquien, $newCiudad, $nombre, $place);
            break;

        case "ModificarEmailLocal":
            $nombre = $_SESSION["userlogin"];
            $perfil = "locales";
            $id_de_aquien = "nombre_local";
            $nuevaPropiedad = $_POST["newEmail"];
            $arroba = '@';
            $pos = strpos($nuevaPropiedad, $arroba);
            if ($pos == true) {
                echo modificarcontactoEmail($perfil, $id_de_aquien, $nuevaPropiedad, $nombre);
            } else {
                echo "Email no valido te falta la @";
            }
            break;
        case "ModificarTelefonoLocal":
            $nombre = $_SESSION["userlogin"];
            $perfil = "locales";
            $id_de_aquien = "nombre_local";
            $nuevaPropiedad = $_POST["newTelefono"];
            $userlogin_local = $_SESSION["userlogin"];
            if (strlen($nuevaPropiedad) === 9 && is_numeric($nuevaPropiedad)) {
                echo modificartelefono($perfil, $id_de_aquien, $nuevaPropiedad, $nombre);
            } else {

                echo 'telefono erroneo';
            }
            break;
        case "ModificarPerfilLocal":
            $nombre = $_SESSION["userlogin"];
            $perfUbicacion = $_POST["perfUbicacion"];
            $perfCiudad = $_POST["perfCiudad"];
            $perfEmail = $_POST["perfEmail"];
            $perfTelefono = $_POST["perfTelefono"];
            $perfAforo = $_POST["perfAforo"];
            echo modificarPerfilLocal($nombre, $perfAforo, $perfUbicacion, $perfCiudad, $perfEmail, $perfTelefono);
            break;
        case "ModificarPasswordLocal":
            $propiedad = "password_local";
            $userlogin_local = $_SESSION["userlogin"];
            $oldPass = $_POST["oldPass"];
            $passBBDD = obtencionContraseñaLocal($userlogin_local);
            while ($fila = mysqli_fetch_array($passBBDD)) {
                extract($fila);
                $password_local;
            }
            $newPass = $_POST["newPass"];
            $newPassHash = password_hash($newPass, PASSWORD_DEFAULT);
            $verificacion = $_POST["Verificar"];
            if ((password_verify($oldPass, $password_local)) && $newPass == $verificacion) {
                echo modificardatosLocal($propiedad, $newPassHash, $userlogin_local);
            } else {
                echo "Contraseña incorrecta";
            }
            break;



//--------------------------------------------------------CREAR CONCIERTO----------------------------------------------------------------------------


        case "CrearConcierto":
            $nombreConcierto = $_POST["nombreConcierto"];
            $genero = $_POST["Estilos"];
            $fechaConcierto = $_POST["FechaConcierto"];
            $time_concierto = $_POST["hora"];
            $estadoConcierto = $_POST["Estado"];
            $precioEntrada = $_POST["PrecioEntrada"];
            $propuestaEconomica = $_POST["PropuestaEconomica"];
            $userlogin_local = $_POST["nombrelocal"];

            echo insertarConciertos($nombreConcierto, $fechaConcierto, $time_concierto, $genero, $estadoConcierto, $precioEntrada, $propuestaEconomica, $userlogin_local);

            break;

//----------------------------------------------INSCRIBIR EN CONCIERTO-------------------------------------------------------------------------------------------------------------//  

        case "inscribirse":
            $id_conciertotravez = $_POST["idconcierto"];
            $nombre_artistico = $_SESSION["userlogin"];
            $fecha_inscripcion = date("Y-m-d H:i:s");
            echo inscribirseenConcierto($id_conciertotravez, $nombre_artistico, $fecha_inscripcion);

            break;




//-------------------------------------------------------------------------MODIFICAR PERFIL FAN-------------------------------------------------------------------------------------------------



        case "ModificarNombreFan":
            $perfil = "fan";
            $id_de_aquien = "nombre_usuario_fan";
            $nuevaPropiedad = $_POST["newNombre"];
            $nombre = $_SESSION["userlogin"];
            echo modificarNombre($perfil, $id_de_aquien, $nuevaPropiedad, $nombre);
            break;
        case "ModificarApellidosFan":
            $perfil = "fan";
            $id_de_aquien = "nombre_usuario_fan";
            $nuevaPropiedad = $_POST["newApellido"];
            $nombre = $_SESSION["userlogin"];
            echo modificarApellidos($perfil, $id_de_aquien, $nuevaPropiedad, $nombre);
            break;
        case "ModificarDireccionFan":
            $perfil = "fan";
            $id_de_aquien = "nombre_usuario_fan";
            $nuevaPropiedad = $_POST["newDireccion"];
            $nombre = $_SESSION["userlogin"];
            $place = "direccion";
            echo modificarubicacion($perfil, $id_de_aquien, $nuevaPropiedad, $nombre, $place);
            break;
        case "ModificarCiudadFan":
            $aquien = "fan";
            $id_de_aquien = "nombre_usuario_fan";
            $propiedad = "id_ciudad";
            $newCiudad = $_POST["newCiudad"];
            $nombre = $_SESSION["userlogin"];
            echo modificarciudad($aquien, $id_de_aquien, $newCiudad, $nombre);
            break;
        case "ModificarEmailFan":
            $perfil = "fan";
            $id_de_aquien = "nombre_usuario_fan";
            $nuevaPropiedad = $_POST["newEmail"];
            $nombre = $_SESSION["userlogin"];
            $arroba = '@';
            $pos = strpos($nuevaPropiedad, $arroba);
            if ($pos == true) {
                echo modificarcontactoEmail($perfil, $id_de_aquien, $nuevaPropiedad, $nombre);
            } else {
                echo "Email no valido te falta la @";
            }
            break;
        case "ModificarTelfFan":
            $perfil = "fan";
            $id_de_aquien = "nombre_usuario_fan";
            $nuevaPropiedad = $_POST["newTelefono"];
            $nombre = $_SESSION["userlogin"];
            if (strlen($nuevaPropiedad) === 9 && is_numeric($nuevaPropiedad)) {
                echo modificarTelefono($perfil, $id_de_aquien, $nuevaPropiedad, $nombre);
            } else {
                echo "Numero no válido";
            }
            break;

        case "ModificarPerfilFan":
            $nombre = $_SESSION["userlogin"];
            $nombreUsuFan = $_POST["perfNombre"];
            $apellidosUsuFan = $_POST["perfApellidos"];
            $direccionUsuFan = $_POST["perfDireccion"];
            $ciudadUsuFan = $_POST["perfCiudad"];
            $emailUsuFan = $_POST["perfEmail"];
            $telefonoUsuFan = $_POST["perfTelefono"];

            echo modificarPerfilFan($nombre, $nombreUsuFan, $apellidosUsuFan, $emailUsuFan, $telefonoUsuFan, $direccionUsuFan, $ciudadUsuFan);
            break;

        case "ModificarPasswordFan":
            $propiedad = "password_fan";
            $userlogin_fan = $_SESSION["userlogin"];
            $oldPass = $_POST["oldPass"];
            $passBBDD = obtencionContraseñaFan($userlogin_fan);
            while ($fila = mysqli_fetch_array($passBBDD)) {
                extract($fila);
                $password_fan;
            }
            $newPass = $_POST["newPass"];
            $newPassHash = password_hash($newPass, PASSWORD_DEFAULT);
            $verificacion = $_POST["Verificar"];
            if ((password_verify($oldPass, $password_fan)) && $newPass == $verificacion) {
                modificardatosFan($propiedad, $newPassHash, $userlogin_fan);
            } else {
                echo "Contraseña incorrecta";
            }
            break;





//-------------------------------------------------------------MODIFICAR PERFIL MUSICO-------------------------------------------------------------------------------


        case "ModificarGéneroMusico";
            $nombre = $_SESSION["userlogin"];
            $perfil = "musico";
            $id_de_aquien = "nombre_artistico";
            $nuevaPropiedad = $_POST["newGenero"];
            echo $resultadoGeneroMusico = modificarGeneroMusico($perfil, $id_de_aquien, $nuevaPropiedad, $nombre);
            break;
        case "ModificarComponentesMusico";
            $nombre = $_SESSION["userlogin"];
            $perfil = "musico";
            $id_de_aquien = "nombre_artistico";
            $nuevaPropiedad = $_POST["newComponentes"];
            echo $resultadoComponentesMusico = modificarcomponentes($perfil, $id_de_aquien, $nuevaPropiedad, $nombre);
            break;
        case "ModificarCiudadMusico";
            $perfil = "musico";
            $id_de_aquien = "nombre_artistico";
            $newCiudad = $_POST["newCiudad"];
            $nombre = $_SESSION["userlogin"];
            echo modificarciudad($perfil, $id_de_aquien, $newCiudad, $nombre);
            break;


        case "ModificarEmailMusico";
            $nombre = $_SESSION["userlogin"];
            $perfil = "musico";
            $id_de_aquien = "nombre_artistico";
            $nuevaPropiedad = $_POST["newEmail"];
            $arroba = '@';
            $pos = strpos($nuevaPropiedad, $arroba);
            if ($pos == true) {
                echo modificarcontactoEmail($perfil, $id_de_aquien, $nuevaPropiedad, $nombre);
            } else {
                echo "Email no valido te falta la @";
            }

            break;
        case "ModificarTelefonoMusico";
            $nombre = $_SESSION["userlogin"];
            $perfil = "musico";
            $id_de_aquien = "nombre_artistico";
            $nuevaPropiedad = $_POST["newTelefono"];
            if (strlen($nuevaPropiedad) === 9 && is_numeric($nuevaPropiedad)) {
                echo modificarTelefono($perfil, $id_de_aquien, $nuevaPropiedad, $nombre);
            } else {
                echo "Numero no válido";
            }
            break;

        case "ModificarNombreMusico":
            $nombre = $_SESSION["userlogin"];
            $perfil = "musico";
            $id_de_aquien = "nombre_artistico";
            $nuevaPropiedad = $_POST["newNombre"];
            echo $resultadoNombreMusico = modificarNombre($perfil, $id_de_aquien, $nuevaPropiedad, $nombre);
            break;
        case "ModificarApellidoMusico":
            $nombre = $_SESSION["userlogin"];
            $perfil = "musico";
            $id_de_aquien = "nombre_artistico";
            $nuevaPropiedad = $_POST["newApellido"];
            echo $resultadoApellidosMusico = modificarApellidos($perfil, $id_de_aquien, $nuevaPropiedad, $nombre);
            break;
        case "ModificarPasswordMusico":
            $propiedad = "password_musico";
            $userlogin_musico = $_SESSION["userlogin"];
            $oldPass = $_POST["oldPass"];
            $passBBDD = obtencionContraseñaMusico($userlogin_musico);
            while ($fila = mysqli_fetch_array($passBBDD)) {
                extract($fila);
                $password_musico;
            }
            $newPass = $_POST["newPass"];
            $newPassHash = password_hash($newPass, PASSWORD_DEFAULT);
            $verificacion = $_POST["Verificar"];
            if ((password_verify($oldPass, $password_musico)) && $newPass == $verificacion) {
                echo modificarDatosMusico($propiedad, $newPassHash, $userlogin_musico);
            } else {
                echo "Contraseña incorrecta";
            }
            break;
        case "ModificarPerfilMusico":
            $nombre = $_SESSION["userlogin"];
            $generoUsuMusico = $_POST["Estilos"];
            $nombreUsuMusico = $_POST["perfNombre"];
            $apellidosUsuMusico = $_POST["perfApellidos"];
            $componentesUsuMusico = $_POST["perfComponentes"];
            $ciudadUsuMusico = $_POST["perfCiudad"];
            $emailUsuMusico = $_POST["perfEmail"];
            $telefonoUsuMusico = $_POST["perfTelefono"];
            $webUsuMusico = $_POST["perfWeb"];
            echo modificarPerfilMusico($nombre, $generoUsuMusico, $nombreUsuMusico, $apellidosUsuMusico, $componentesUsuMusico, $ciudadUsuMusico, $emailUsuMusico, $telefonoUsuMusico, $webUsuMusico);
            break;
        case "ModificarWebMusico":
            $nombre = $_SESSION["userlogin"];
            $perfil = "musico";
            $id_de_aquien = "nombre_artistico";
            $nuevaPropiedad = $_POST["newWeb"];
            echo modificarWeb($perfil, $id_de_aquien, $nuevaPropiedad, $nombre);
            break;
    }
}

//****************************************************************INSERT**************************************************************

function insertarConciertos($nombreConcierto, $fechaConcierto, $time_concierto, $genero, $estadoConcierto, $precioEntrada, $propuestaEconomica, $userlogin_local) {
    $c = conectar();
    $insert = "insert into concierto(id_concierto,nombre_concierto, fecha_concierto,time_concierto, genero, estado, precio_entrada, propuesta_economica, nombre_local)"
            . "values(NULL,'$nombreConcierto','$fechaConcierto','$time_concierto','$genero','$estadoConcierto','$precioEntrada','$propuestaEconomica','$userlogin_local')";
    mysqli_query($c, $insert);


    if (mysqli_affected_rows($c)) {
        $resultado = "Concierto creado correctamente";
    } else {
        $resultado = mysqli_error($c);
    }
    desconectar($c);
    return $resultado;
}

function inscribirseenConcierto($id_concierto, $nombre_artistico, $fecha_inscripcion) {
    $c = conectar();

    $insert = "insert into inscribir(id_concierto,nombre_artistico, fecha_inscripcion)"
            . "values('$id_concierto','$nombre_artistico','$fecha_inscripcion')";

    mysqli_query($c, $insert);

    if (mysqli_affected_rows($c)) {

        $resultado = "Inscrito  en este concierto correctamente";
    } else {
        $resultado = mysqli_error($c);
    }
    desconectar($c);
    return $resultado;
}

/* * ************************************************************UPDATE************************************************************************** */

function modificarciudad($perfil, $id_de_aquien, $nuevaPropiedad, $nombre) {

    $c = conectar();
    $update = "update $perfil set id_ciudad=(select id_ciudad from ciudad where municipio='$nuevaPropiedad') where $id_de_aquien='$nombre'";
    mysqli_query($c, $update);

    $select = "select municipio from ciudad inner join $perfil on $perfil.id_ciudad=ciudad.id_ciudad where $id_de_aquien='$nombre'";
    $result = mysqli_query($c, $select);

    desconectar($c);

    $row = mysqli_fetch_array($result)[0];
    return $row;
}

function modificarubicacion($perfil, $id_de_aquien, $nuevaPropiedad, $nombre, $place) {

    $c = conectar();
    $update = "update $perfil set $place=('$nuevaPropiedad') where $id_de_aquien='$nombre'";
    mysqli_query($c, $update);
    $select = "select $place from $perfil where $id_de_aquien='$nombre'";
    $result = mysqli_query($c, $select);
    desconectar($c);
    $row = mysqli_fetch_array($result)[0];
    return $row;
}

function modificarcontactoEmail($perfil, $id_de_aquien, $nuevaPropiedad, $nombre) {

    $c = conectar();
    $update = "update $perfil set email=('$nuevaPropiedad') where $id_de_aquien='$nombre'";
    mysqli_query($c, $update);

    $select = "select email from $perfil where $id_de_aquien='$nombre'";
    $result = mysqli_query($c, $select);

    desconectar($c);
    $row = mysqli_fetch_array($result)[0];
    return $row;
}

function modificarNombre($perfil, $id_de_aquien, $nuevaPropiedad, $nombre) {

    $c = conectar();
    $update = "update $perfil set nombre=('$nuevaPropiedad') where $id_de_aquien='$nombre'";
    mysqli_query($c, $update);

    $select = "select nombre from $perfil where $id_de_aquien='$nombre'";
    $result = mysqli_query($c, $select);

    desconectar($c);
    $row = mysqli_fetch_array($result)[0];
    return $row;
}

function modificarApellidos($perfil, $id_de_aquien, $nuevaPropiedad, $nombre) {

    $c = conectar();
    $update = "update $perfil set apellidos=('$nuevaPropiedad') where $id_de_aquien='$nombre'";
    mysqli_query($c, $update);

    $select = "select apellidos from $perfil where $id_de_aquien='$nombre'";
    $result = mysqli_query($c, $select);

    desconectar($c);
    $row = mysqli_fetch_array($result)[0];
    return $row;
}

function modificarWeb($perfil, $id_de_aquien, $nuevaPropiedad, $nombre) {

    $c = conectar();
    $update = "update $perfil set web=('$nuevaPropiedad') where $id_de_aquien='$nombre'";
    mysqli_query($c, $update);

    $select = "select web from $perfil where $id_de_aquien='$nombre'";
    $result = mysqli_query($c, $select);

    desconectar($c);
    $row = mysqli_fetch_array($result)[0];
    return $row;
}

function modificarTelefono($perfil, $id_de_aquien, $nuevaPropiedad, $nombre) {

    $c = conectar();
    $update = "update $perfil set telefono=('$nuevaPropiedad') where $id_de_aquien='$nombre'";
    mysqli_query($c, $update);

    $select = "select telefono from $perfil where $id_de_aquien='$nombre'";
    $result = mysqli_query($c, $select);

    desconectar($c);
    $row = mysqli_fetch_array($result)[0];
    return $row;
}

function modificarcomponentes($perfil, $id_de_aquien, $nuevaPropiedad, $nombre) {

    $c = conectar();
    $update = "update $perfil set numero_componentes=('$nuevaPropiedad') where $id_de_aquien ='$nombre'";
    mysqli_query($c, $update);

    $select = "select numero_componentes from $perfil where $id_de_aquien='$nombre'";
    $result = mysqli_query($c, $select);

    desconectar($c);

    $row = mysqli_fetch_array($result)[0];
    return $row;
}

function modificarGeneroMusico($perfil, $id_de_aquien, $nuevaPropiedad, $nombre) {

    $c = conectar();
    $update = "update $perfil set genero=('$nuevaPropiedad') where $id_de_aquien='$nombre'";
    mysqli_query($c, $update);

    $select = "select genero from $perfil where $id_de_aquien='$nombre'";
    $result = mysqli_query($c, $select);

    desconectar($c);
    $row = mysqli_fetch_array($result)[0];
    return $row;
}

//----------------------modificacion de perfiles de una vez---------------------------------------------------

function modificarPerfilFan($nombre, $nombreUsuFan, $apellidosUsuFan, $emailUsuFan, $telefonoUsuFan, $direccionUsuFan, $ciudadUsuFan) {
    $c = conectar();
    $update = "update fan set nombre='$nombreUsuFan', apellidos='$apellidosUsuFan', email='$emailUsuFan', telefono='$telefonoUsuFan', direccion='$direccionUsuFan', id_ciudad=(select id_ciudad from ciudad where municipio='$ciudadUsuFan') where nombre_usuario_fan='$nombre'";
    mysqli_query($c, $update);

    if (mysqli_affected_rows($c)) {
        $resultado = "Perfil actualizado correctamente";
    } else {
        $resultado = "Los datos introducidos no son correctos";
    }
    desconectar($c);
    return $resultado;
}

function modificarPerfilMusico($nombre, $generoUsuMusico, $nombreUsuMusico, $apellidosUsuMusico, $componentesUsuMusico, $ciudadUsuMusico, $emailUsuMusico, $telefonoUsuMusico, $webUsuMusico) {
    $c = conectar();
    $update = "update musico set nombre='$nombreUsuMusico',genero='$generoUsuMusico',numero_componentes='$componentesUsuMusico',apellidos='$apellidosUsuMusico',web='$webUsuMusico', id_ciudad=(select id_ciudad from ciudad where municipio='$ciudadUsuMusico'), email='$emailUsuMusico', telefono='$telefonoUsuMusico'where nombre_artistico='$nombre'";
    mysqli_query($c, $update);

    if (mysqli_affected_rows($c)) {
        $resultado = "Perfil actualizada correctamente";
    } else {
        $resultado = "Los datos introducidos no son correctos";
    }
    desconectar($c);
    return $resultado;
}

function modificarPerfilLocal($nombre, $perfAforo, $perfUbicacion, $perfCiudad, $perfEmail, $perfTelefono) {

    $c = conectar();
    $update = "update locales set ubicacion='$perfUbicacion',email='$perfEmail',id_ciudad=(select id_ciudad from ciudad where municipio='$perfCiudad'),telefono='$perfTelefono',aforo='$perfAforo' where nombre_local='$nombre'";
    mysqli_query($c, $update);
    if (mysqli_affected_rows($c)) {
        $resultado = "Actualizado correctamente";
    } else {
        $resultado = "Los datos introducidos no son correctos";
    }

    desconectar($c);

    return $resultado;
}

//------------------------updates generales------------------------------------------------------------------------------------------------------------------------

function modificarDatosMusico($propiedad, $nuevaPropiedad, $userlogin_musico) {
    $c = conectar();
    $update = "update musico set $propiedad='$nuevaPropiedad' where nombre_artistico='$userlogin_musico'";
    mysqli_query($c, $update);

// Comprobamos que con la sentencia de sql que hemos hecho ha afectado a alguna fila, si es que si devuelve una respuesta de éxito
//si no es así, devuelve un error de datos introducidos.
    if (mysqli_affected_rows($c)) {
        $resultado = "Actualizado correctamente";
    } else {
        $resultado = "Los datos introducidos no son correctos";
    }

    desconectar($c);
    return $resultado;
}

function modificarDatosLocal($propiedad, $nuevaPropiedad, $userlogin_musico) {
    $c = conectar();
    $update = "update locales set $propiedad='$nuevaPropiedad' where nombre_local='$userlogin_musico'";
    mysqli_query($c, $update);

// Comprobamos que con la sentencia de sql que hemos hecho ha afectado a alguna fila, si es que si devuelve una respuesta de éxito
//si no es así, devuelve un error de datos introducidos.
    if (mysqli_affected_rows($c)) {
        $resultado = "Actualizado correctamente";
    } else {
        $resultado = "Los datos introducidos no son correctos";
    }

    desconectar($c);
    return $resultado;
}

function modificardatosFan($propiedad, $nuevaPropiedad, $userlogin_fan) {

    $c = conectar();
    $update = "update fan set $propiedad='$nuevaPropiedad' where nombre_usuario_fan='$userlogin_fan'";
    mysqli_query($c, $update);

// Comprobamos que con la sentencia de sql que hemos hecho ha afectado a alguna fila, si es que si devuelve una respuesta de éxito
//si no es así, devuelve un error de datos introducidos.
    if (mysqli_affected_rows($c)) {
        $resultado = "Actualizado correctamente";
    } else {
        $resultado = "Los datos introducidos no son correctos";
    }
    desconectar($c);
    return $resultado;
}

//-------------------------------------------------------------------------------------------------------------OBTEN CONTRASEÑA--------------------------------------------------------------------------------------

function obtencionContraseñaMusico($userlogin_musico) {
    $c = conectar();
    $select = "select password_musico from musico where nombre_artistico = '$userlogin_musico'";
    $resultado = mysqli_query($c, $select);
    desconectar($c);
    return $resultado;
}

function obtencionContraseñaLocal($userlogin_local) {
    $c = conectar();
    $select = "select password_local from locales where nombre_local = '$userlogin_local'";
    $resultado = mysqli_query($c, $select);
    desconectar($c);
    return $resultado;
}

function obtencionContraseñaFan($userlogin_fan) {
    $c = conectar();
    $select = "select password_fan from fan where nombre_usuario_fan = '$userlogin_fan'";
    $resultado = mysqli_query($c, $select);
    desconectar($c);
    return $resultado;
}

//*****************************************************************SELECT**************************************************************
function mostrarDatosUsuarioFan($userlogin_fan) {
    $c = conectar();
    mysqli_set_charset($c, "utf8");
    $select = "select * from fan inner join ciudad "
            . "on fan.id_ciudad=ciudad.id_ciudad where nombre_usuario_fan= '$userlogin_fan'";
    $resultado = mysqli_query($c, $select);
    desconectar($c);
    return $resultado;
}

function mostrarDatosUsuarioMusico($sesionUsuMusico) {
    $c = conectar();
    mysqli_set_charset($c, "utf8");
    $select = "select nombre_artistico, genero, numero_componentes,nombre,apellidos,telefono,municipio,email,web, imagen "
            . "from musico inner join ciudad "
            . "on musico.id_ciudad=ciudad.id_ciudad where nombre_artistico = '$sesionUsuMusico'";
    $resultado = mysqli_query($c, $select);
    desconectar($c);
    return $resultado;
}

function mostrarDatosUsuarioLocal($userlogin_local) {
    $c = conectar();
    mysqli_set_charset($c, "utf8");
    $select = "select nombre_local, ubicacion,email,telefono,municipio,aforo,imagen
                from locales
                inner join ciudad
                on locales.id_ciudad=ciudad.id_ciudad 
                where nombre_local = '$userlogin_local'";
    $resultado = mysqli_query($c, $select);
    desconectar($c);
    return $resultado;
}

function select1() {
    $c = conectar();
    mysqli_set_charset($c, "utf8");
    $propiedad = 'select nombre_artistico, fecha_concierto, precio_entrada, nombre_local from concierto';
    $result = mysqli_query($c, $propiedad);
    desconectar($c);
    return $result;
}

function select2() {
    $c = conectar();
    mysqli_set_charset($c, "utf8");
    $propiedad = 'select nombre_artistico, genero, nombre, web from musico';
    $result = mysqli_query($c, $propiedad);
    desconectar($c);
    return $result;
}

function selectConciertos($userlogin_local) {
    $c = conectar();
    mysqli_set_charset($c, "utf8");
    $propiedad = "select nombre_concierto, fecha_concierto,time_concierto, genero, estado, precio_entrada,propuesta_economica,nombre_artistico from proyecto.concierto where nombre_local='$userlogin_local'";
    $result = mysqli_query($c, $propiedad);
    desconectar($c);
    return $result;
}

function select_genero_musical() {
    $c = conectar();
    mysqli_set_charset($c, "utf8");
    $select = "select genero from genero_musical";
    $result = mysqli_query($c, $select);
    desconectar($c);
    return $result;
}

function select_ciudades() {
    $c = conectar();
    mysqli_set_charset($c, "utf8");
    $select = "select municipio from proyecto.ciudad";
    $result = mysqli_query($c, $select);
    desconectar($c);
    return $result;
}

function select_de_conciertos_inscritos($nombre_artistico) {
    $c = conectar();
    mysqli_set_charset($c, "utf8");
    $select = "select concierto.id_concierto,nombre_concierto,propuesta_economica, fecha_inscripcion,nombre_local,respuesta_inscripcion from concierto inner join inscribir on concierto.id_concierto=inscribir.id_concierto where inscribir.id_concierto in(select id_concierto from inscribir where nombre_artistico='$nombre_artistico')";
    $result = mysqli_query($c, $select);
    desconectar($c);
    return $result;
}

function select_conciertos_para_inscribirse($nombre_artistico) {
    $c = conectar();
    mysqli_set_charset($c, "utf8");
    $select = "select id_concierto, nombre_concierto,propuesta_economica,estado,
fecha_concierto,time_concierto,nombre_local from concierto
 where genero=(select genero from musico where nombre_artistico='$nombre_artistico') and
 estado='0'";
    $result = mysqli_query($c, $select);
    desconectar($c);
    return $result;
}

function selectConciertosInscritos($nombre_artistico) {
    $c = conectar();
    $select = " select concierto.id_concierto, fecha_inscripcion,fecha_concierto, respuesta_inscripcion,nombre_concierto
     from inscribir
     inner join concierto on concierto.id_concierto=inscribir.id_concierto  where inscribir.nombre_artistico='$nombre_artistico'";
    $result = mysqli_query($c, $select);
    desconectar($c);
    return $result;
}

function inscrito_a_concierto($id_concierto, $nombre_artistico) {
    $c = conectar();
    mysqli_set_charset($c, "utf8");
    $select = "select * from inscribir where id_concierto=$id_concierto and nombre_artistico='$nombre_artistico'";
    $result = mysqli_query($c, $select);
    desconectar($c);
    return mysqli_num_rows($result) < 1;
}

function select_conciertos_creados_por_local($userlogin_local) {
    $c = conectar();
    mysqli_set_charset($c, "utf8");
    $select = "select concierto.id_concierto,nombre_concierto, fecha_concierto,time_concierto, genero, estado, precio_entrada,propuesta_economica,nombre_artistico from concierto where nombre_local='$userlogin_local' ";
    $result = mysqli_query($c, $select);
    desconectar($c);
    return $result;
}

function selectConciertosInscritos_por_Local($userlogin_local) {
    $c = conectar();
    mysqli_set_charset($c, "utf8");
    $select = "select concierto.id_concierto,nombre_concierto, fecha_concierto,time_concierto, genero, estado, precio_entrada,propuesta_economica,nombre_artistico from concierto where nombre_local='$userlogin_local' and estado=1";
    $result = mysqli_query($c, $select);
    desconectar($c);
    return $result;
}

function select_de_todos_los_locales() {
    $c = conectar();
    mysqli_set_charset($c, "utf8");
    $select = ' select nombre_local, ubicacion,municipio,telefono from locales
 inner join ciudad on
 ciudad.id_ciudad=locales.id_ciudad;';
    $result = mysqli_query($c, $select);
    desconectar($c);
    return $result;
}

function select_de_todos_los_artistas() {
    $c = conectar();
    mysqli_set_charset($c, "utf8");
    $select = 'select nombre_artistico,genero,nombre,apellidos,web from musico';
    $result = mysqli_query($c, $select);
    desconectar($c);
    return $result;
}

function select_de_votos_Conciertos() {
    $c = conectar();
    mysqli_set_charset($c, "utf8");
    $select = "select nombre_concierto, count(*) from puntuar 
 inner join concierto on
 puntuar.id_concierto=concierto.id_concierto
 group by puntuar.id_concierto";
    $result = mysqli_query($c, $select);
    desconectar($c);
    return $result;
}

function select_de_votos_Musicos() {
    $c = conectar();
    mysqli_set_charset($c, "utf8");
    $select = "select nombre_artistico , count(nombre_artistico) from votar group by nombre_artistico ";
    $result = mysqli_query($c, $select);
    desconectar($c);
    return $result;
}

function mostrarConciertosFan() {
    $c = conectar();
    mysqli_set_charset($c, "utf8");
    $select = "select * from concierto";
    $resultado = mysqli_query($c, $select);
    desconectar($c);
    return $resultado;
}

function mostrarMusicosFan() {
    $c = conectar();
    mysqli_set_charset($c, "utf8");
    $select = "select * from musico";
    $resultado = mysqli_query($c, $select);
    desconectar($c);
    return $resultado;
}

function votarConcierto($nombreUsuarioFan, $idConcierto) {
    $c = conectar();
    $select = "INSERT into puntuar VALUES('$nombreUsuarioFan', '$idConcierto')";
    mysqli_query($c, $select);
    $result = mysqli_affected_rows($c) >= 1;
    desconectar($c);
    return $result;
}

function quitarVotoConcierto($nombreUsuarioFan, $idConcierto) {
    $c = conectar();
    $select = "DELETE FROM puntuar where nombre_usuario_fan='$nombreUsuarioFan' AND id_concierto='$idConcierto'";
    mysqli_query($c, $select);

    $result = mysqli_affected_rows($c) >= 1;
    desconectar($c);
    return $result;
}

function comprobarVotoMusico($nombreUsuarioFan, $nombreMusico) {
    $c = conectar();
    mysqli_set_charset($c, "utf8");
    $select = "SELECT * FROM votar where nombre_usuario_fan='$nombreUsuarioFan' AND nombre_artistico='$nombreMusico'";
    mysqli_query($c, $select);
    $result = mysqli_affected_rows($c) >= 1;
    desconectar($c);
    return $result;
}

function comprobarVotoConcierto($nombreUsuarioFan, $id_concierto) {
    $c = conectar();
    mysqli_set_charset($c, "utf8");
    $select = "SELECT * FROM puntuar where nombre_usuario_fan='$nombreUsuarioFan' AND id_concierto='$id_concierto'";
    mysqli_query($c, $select);
    $result = mysqli_affected_rows($c) >= 1;
    desconectar($c);
    return $result;
}

function votarMusico($nombreUsuarioFan, $nombreMusico) {
    $c = conectar();
    $select = "INSERT into votar VALUES('$nombreUsuarioFan', '$nombreMusico')";
    mysqli_query($c, $select);
    $result = mysqli_affected_rows($c) >= 1;
    desconectar($c);
    return $result;
}

function quitarVotoMusico($nombreUsuarioFan, $nombreMusico) {
    $c = conectar();
    $select = "DELETE FROM votar where nombre_usuario_fan='$nombreUsuarioFan' AND nombre_artistico='$nombreMusico'";
    mysqli_query($c, $select);
    $result = mysqli_affected_rows($c) >= 1;
    desconectar($c);
    return $result;
}

function losMusicosMasVotados() {
    $c = conectar();
    mysqli_set_charset($c, "utf8");
    $select = "SELECT votar.nombre_artistico,genero, COUNT(votar.nombre_artistico) 'votos' FROM votar
            inner join musico on musico.nombre_artistico=votar.nombre_artistico GROUP BY nombre_artistico ORDER BY COUNT(votar.nombre_artistico) DESC LIMIT 10";
    $resultado = mysqli_query($c, $select);
    desconectar($c);
    return $resultado;
}

function borrarseConcierto($id_concierto, $nombre_musico) {
    $c = conectar();
    $delete = "delete from inscribir where id_concierto='$id_concierto' and nombre_artistico='$nombre_musico'";
    mysqli_query($c, $delete);
    $resultado = mysqli_affected_rows($c) >= 1;
    desconectar($c);
    return $resultado;
}

function proximosConciertos() {
    $c = conectar();
    mysqli_set_charset($c, "utf8");
    $select = "select nombre_concierto,nombre_local,nombre_artistico,fecha_concierto,genero from concierto";
    $resultado = mysqli_query($c, $select);
    desconectar($c);
    return $resultado;
}

// Función que nos devuelve los conciertos
// desde una posición y una cantidad determinada
function selectConciertosDesde($inicio, $cantidad) {
    $c = conectar();
    mysqli_set_charset($c, "utf8");
    $select = "select * from concierto where nombre_artistico is not null limit $inicio, $cantidad";
    $result = mysqli_query($c, $select);
    desconectar($c);
    return $result;
}

// Función que devuelve cuántos platos hay en la bbdd
function totalConciertos() {
    $c = conectar();
    $select = "select count(*) as cantidad from concierto where nombre_artistico is not null";
    $result = mysqli_query($c, $select);
    $fila = mysqli_fetch_assoc($result);
    desconectar($c);
    return $fila["cantidad"];
}

//Funcion que nos devuelve los musicos desde una posicion y una cantidad determinada
function selectMusicoDesde($inicio, $cantidad) {
    $c = conectar();
    mysqli_set_charset($c, "utf8");
    $select = "select * from musico limit $inicio, $cantidad";
    $result = mysqli_query($c, $select);
    desconectar($c);
    return $result;
}

// Función que devuelve cuántos musicos hay en la bbdd
function totalMusicos() {
    $c = conectar();
    $select = "select count(*) as cantidad from musico";
    $result = mysqli_query($c, $select);
    $fila = mysqli_fetch_assoc($result);
    desconectar($c);
    return $fila["cantidad"];
}

function selectMusicoX_idConcierto($id_concierto, $userlogin_local) {
    $c = conectar();
    $select = "select inscribir.id_concierto,inscribir.nombre_artistico,numero_componentes,telefono,web
                from inscribir
                inner join musico
                on inscribir.nombre_artistico=musico.nombre_artistico
                inner join concierto
                on inscribir.id_concierto=concierto.id_concierto
                where inscribir.id_concierto='$id_concierto' and nombre_local='$userlogin_local' and inscribir.respuesta_inscripcion!='asignado'";
//echo $select;
    $resultado = mysqli_query($c, $select);
    desconectar($c);
    return $resultado;
}

function Update_musico_Asignado($nombre_artistico, $id_concierto) {
    $c = conectar();
    $update1 = "update concierto set nombre_artistico='$nombre_artistico' where id_concierto='$id_concierto'";
    mysqli_query($c, $update1);
    $update2 = "update concierto set estado=1 where id_concierto='$id_concierto'";
    $resultado = mysqli_query($c, $update2);
    desconectar($c);
    return $resultado;
}
