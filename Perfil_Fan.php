<?php
session_start();
require_once './bbdd_perfiles.php';
if (isset($_SESSION["userlogin"])) {
    $userlogin_fan = $_SESSION["userlogin"];
    ?>

    <html class="htmlFans">
        <head>
            <meta charset="UTF-8">
            <title></title>
            <link href="CSS/CSS_Perfiles.css" rel="stylesheet" type="text/css"/>
            <script src="JAVASCRIPT/pluguinJquery.js" type="text/javascript"></script>
            <script src="plugin/dist/jquery.validate.js" type="text/javascript"></script>
            <script src="JAVASCRIPT/JS_Perfiles.js" type="text/javascript"></script>
            <script src="JAVASCRIPT/jquery.numeric.js" type="text/javascript"></script>
        </head>
        <body class="bodyFans">
            <section class="PrincipalHeader">
                <div class="IntroPagina">
                    <div class="ContenidoHeader">
                        <h1>ZONA<br/> FANS</h1>
                        <a class="Boton" href="#Scroll">Entra</a>
                    </div>

                </div>
            </section>

            <div id="Scroll"></div>
            <section class="Body2">
                <div class="ContenidoIzquierda">
                    <div id="Perfil">

                        <?php
                        $nombreUsuFan = mostrarDatosUsuarioFan($userlogin_fan);
                        while ($fila = mysqli_fetch_array($nombreUsuFan)) {
                            extract($fila);
                            echo '<img src =' . "usuariosFotos2/" . $imagen . ' class = "Avatar">';
                            echo "<h1 id='nomUSU'>$nombre_usuario_fan</h1>"
                            . "<label style='color:green'>Nombre:</label><label id='nombre'>$nombre</label><br/>"
                            . "<label style='color:green'>Apellidos:</label><label id='apellidos'>$apellidos</label><br/>"
                            . "<label style='color:green'>Email:</label><label id='email'>$email</label><br/>"
                            . "<label style='color:green'>Telefono:</label><label id='telefono'>$telefono</label><br/>"
                            . "<label style='color:green'>Dirección:</label><label id='direccion'>$direccion</label><br/>"
                            . "<label style='color:green'>Ciudad:</label><label id='municipio'>$municipio</label><br/>";
                        }
                        ?>
                        <br/>
                        <a id="Boton4" href="LOGOUT.php" >Salir</a>
                        
                    </div>
                </div>
                <div class="ContenidoDerecha">
                    <ul>
                        <li><a>Modificar</a>
                            <ul>
                                <li id="BotNom"><a>Nombre</a></li>
                                <li id="BotApe"><a>Apellidos</a></li>
                                <li id="BotUbi"><a>Ubicación</a></li>
                                <li id="BotCiu"><a>Ciudad</a></li>
                                <li id="BotCont"><a>Contacto</a></li>
                                <li id="BotTelf"><a>Telefono</a></li>
                                <li id="BotPerf"><a>Perfil</a></li>
                                <li id="BotPass"><a>Contraseña</a></li>
                            </ul>
                        </li>
                        <li id="BotListConc"><a>Conciertos</a></li>
                        <li id="ConciertosAsignados"><a>Músicos</a></li>
                    </ul>
                    <div class="DivForm">
                        <form class="ModNom" method="POST">
                            <h1>MODIFICAR NOMBRE</h1>
                            <input type="text" name="newNombre" placeholder="Nombre nuevo"/><br/>
                            <input type="button" value="Modificar Nombre" name="ENVIAR1" id="EnviarNombreFan"/>
                            <div id="respuesta">Modificado correctamente</div>
                        </form>
                        <form class="ModApellido" method="POST">
                            <h1>MODIFICAR APELLIDOS</h1>
                            <input type="text" name="newApellido" placeholder="Apellidos nuevos"/><br/>
                            <input type="button" value="Modificar Apellidos" name="ENVIAR1" id="EnviarApellidosFan"/>
                            <div id="respuesta2">Modificado correctamente</div>
                        </form>
                        <form class="ModUbi" method="POST">
                            <h1>MODIFICAR DIRECCIÓN</h1>
                            <input type="text" name="newDireccion" placeholder="Ubicación nueva"/><br/>
                            <input type="button" value="Modificar Direccion"  name="ENVIAR2" id="EnviarDireccionFan"/>
                            <div id="respuesta3">Modificado correctamente</div>
                        </form>
                        <form class="ModCiu" method="POST">
                            <h1>MODIFICAR CIUDAD</h1>

                            <?php
                            $todo_ciudades = select_ciudades();
                            echo "<select class='DespGenero' name='newCiudad'>";
                            while ($fila = mysqli_fetch_array($todo_ciudades)) {
                                extract($fila);
                                echo
                                "<option value='$municipio'>$municipio</option>";
                            }

                            echo" </select>";
                            ?>
                            <input type="submit" name="ENVIAR3" id="EnviarCiudadFan"/>
                            <div id="respuesta4">Modificado correctamente</div>

                        </form>
                        <form class="ModContact" method="POST">
                            <h1>MODIFICAR CONTACTO</h1>
                            <input type="email" name="newEmail" pattern="" placeholder="Email nuevo"/><br/>
                            <!--<input type="email" name="newEmail" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}" placeholder="Email nuevo"/><br/>-->
                            <input type="button" value="Modificar Email"  name="ENVIAR" id="EnviarEmailFan"/>
                            <div id="respuesta5">Modificado correctamente</div>
                        </form>
                        <form class="ModTelf" method="POST" id="modificarTelefono">
                            <h1>MODIFICAR TELEFONO</h1>
                            <input class="numeric"  type="number" name="newTelefono" id="newTelefono" placeholder="Telefono nuevo"  maxlength="9"/><br/>
                            <input type="button" value="Modificar Telefono"  name="ENVIAR" id="EnviarTelefonoFan"/>
                            <div id="respuesta6">Modificado correctamente</div>
                        </form>
                        <form class="ModPerfil" method="POST">
                            <h1>MODIFICAR PERFIL</h1>
                            <input type="text" name="perfNombre" placeholder="Modificar nombre"/><br/>
                            <input type="text" name="perfApellidos" placeholder="Modificar apellidos"/><br/>
                            <input type="text" name="perfDireccion" placeholder="Modificar dirección"/><br/>
                            <?php
                            //desplegable de las ciudades de la bbdd
                            $todo_ciudades = select_ciudades();
                            echo "<select  required aria-required=true class='DespGenero' name='perfCiudad'>";
                            while ($fila = mysqli_fetch_array($todo_ciudades)) {
                                extract($fila);
                                echo
                                "<option value='$municipio'>$municipio</option>";
                            }
                            echo" </select>";
                            ?>
                            <input type="email" name="perfEmail" placeholder="Modificar email"/><br/>
                            <input type="tel" name="perfTelefono" placeholder="Modificar telefono"/><br/>
                            <input type="submit" name="ENVIAR" id="EnviarPerfilFan"/>
                            <div id="respuesta7">Modificado correctamente</div>
                        </form>
                        <form class="ModPass" method="POST">
                            <h1>MODIFICAR CONTRASEÑA</h1>
                            <input type="password" name="oldPass" placeholder="Contraseña actual"/><br/>
                            <input type="password" name="newPass" placeholder="Contraseña nueva"/><br/>
                            <input type="password" name="Verificar" placeholder="Verificar"/><br/>
                            <input type="submit" name="ENVIAR" id="EnviarPassFan"/>
                            <div id="respuesta8">Modificado correctamente</div>

                        </form>

                        <?php
                        if (isset($_GET["action"])) {

                            $action = $_GET["action"];

                            if ($action === "votarMusico") {
                                $conciertoId = $_GET["musicoId"];
                                votarMusico($userlogin_fan, $conciertoId);
                            } else if ($action === "quitarVotoMusico") {
                                $conciertoId = $_GET["musicoId"];
                                quitarVotoMusico($userlogin_fan, $conciertoId);
                            } else if ($action === "votarConcierto") {
                                $conciertoId = $_GET["conciertoId"];
                                votarConcierto($userlogin_fan, $conciertoId);
                            } else if ($action === "quitarVotoConcierto") {
                                $conciertoId = $_GET["conciertoId"];
                                quitarVotoConcierto($userlogin_fan, $conciertoId);
                            }
                        }
                        ?>

                        <div class="ConciertosAsignados">
                            <h1  style="color:white">VOTAR MÚSICOS</h1>
                            <?php
                            $total = totalMusicos();
                            $filasPorPagina = 5;

                            if (isset($_GET["contador"])) {
                                $contador = $_GET["contador"];
                            } else {
                                $contador = 0;
                            }

//                            $musicosFan = mostrarMusicosFan();

                            $musicosFanPaginado = selectMusicoDesde($contador, $filasPorPagina);
                            echo "<table style='border:1px'>";

                            echo "<th>Nombre artista</th>";
                            echo "<th>Genero</th>";
                            echo "<th>Numero de componentes</th>";
                            while ($fila = mysqli_fetch_array($musicosFanPaginado)) {
                                extract($fila);

                                echo "<tr>"
                                . "<td>$nombre_artistico</td>"
                                . "<td>$genero</td>"
                                . "<td>$numero_componentes</td>";
                                if (comprobarVotoMusico($userlogin_fan, $nombre_artistico)) {
                                    echo "<td style='background-color:black'><a style='color:red;'  href='Perfil_Fan.php?action=quitarVotoMusico&musicoId=$nombre_artistico'> Quitar voto </a></td>";
                                } else {
                                    echo " <td style='background-color:green'><a style='color:white' href='Perfil_Fan.php?action=votarMusico&musicoId=$nombre_artistico'> Votar </a></td>";
                                }
                            }

                            echo "</table>";
                            
                            
        //                      if (!isset($fila)) {
      //                          echo "<div  id='respuesta15'>";
    //                            echo "No hay músicos que mostrar";
  //                              echo "</div>";
//                            }
                            if ($contador > 0) {
                                echo "<a href='Perfil_Fan.php?contador2=" . ($contador - $filasPorPagina) . "'>Anterior </a>";
                            }

                         
                            ?>
                            <div style='color:white' >
                                <?php
                                if (($contador + $filasPorPagina) <= $total) {
                                    echo "Mostrando de " . ($contador + 1) . " a " . ($contador + $filasPorPagina) . " de $total";
                                } else {
                                    echo "Mostrando de " . ($contador + 1) . " a $total de $total";
                                }
                                if (($contador + $filasPorPagina) < $total) {
                                    echo "<a  href='Perfil_Fan.php?contador=" . ($contador + $filasPorPagina) . "'> Siguiente</a>";
                                }
                                ?>
                            </div>
                        </div>

                        <div class="ListaConciertos">
                            <h1 style="color:white">PUNTUAR CONCIERTOS</h1>

                            <?php
                            $total2 = totalConciertos();
                            $filasPorPagina2 = 5;

                            if (isset($_GET["contador2"])) {
                                $contador2 = $_GET["contador2"];
                            } else {
                                $contador2 = 0;
                            }

                            $conciertosFan2 = selectConciertosDesde($contador2, $filasPorPagina2);

                            echo "<table><tr>";

                            echo "<th>Nombre artista</th>";
                            echo "<th>Nombre concierto</th>";
                            echo "<th>Fecha concierto</th>";
                            echo "<th>Genero</th>";
                            echo "<th>Precio entrada</th>";
                            echo "<th>Nombre Local</th>";
                            echo "<th>Votar</th>";

                            while ($fila2 = mysqli_fetch_array($conciertosFan2)) {
                                extract($fila2);
                                echo "<tr>";
                                echo "<td>$nombre_artistico</td>";
                                echo "<td>$nombre_concierto</td>";
                                echo "<td>$fecha_concierto</td>";
                                echo "<td>$genero</td>";
                                echo "<td>$precio_entrada</td>";
                                echo "<td>$nombre_local</td>";

                                if (comprobarVotoConcierto($userlogin_fan, $id_concierto)) {
                                    echo "<td style='background-color:grey' > <a  style='color:white' href='Perfil_Fan.php?action=quitarVotoConcierto&conciertoId=$id_concierto'>Unlike</a> </td>";
                                } else {
                                    echo "<td style='background-color:green' > <a style='color:white' href='Perfil_Fan.php?action=votarConcierto&conciertoId=$id_concierto'>Like</a> </td>";
                                }
                            }

                            echo "</tr>";
                            echo "</table>";
                            // if (!isset($fila)) {
                             //   echo "<div id='respuesta15'>";
                              //  echo "No hay conciertos que mostrar";
                               // echo "</div>";
                           // }
                            echo "<div style='color:white' >";                    
                             
                            if ($contador2 > 0) {
                                echo "<a href='Perfil_Fan.php?contador2=" . ($contador2 - $filasPorPagina2) . "'>Anterior </a>";
                            }

                            if (($contador2 + $filasPorPagina2) <= $total2) {
                                echo "Mostrando de " . ($contador2 + 1) . " a " . ($contador2 + $filasPorPagina2) . " de $total2";
                            } else {
                                echo "Mostrando de " . ($contador2 + 1) . " a $total2 de $total2";
                            }
                            if (($contador2 + $filasPorPagina2) < $total2) {
                                echo "<a href='Perfil_Fan.php?contador=" . ($contador2 + $filasPorPagina2) . "'> Siguiente</a>";
                            }

                          
                            ?> 

                        </div>
                    </div>
                </div>    
            </div>
            <div id="Barra">
                <p>CONTACTO: <br/>
                    c/Sant Quirze, 46-49, 2º 1º A<br/>
                    08005 - Barcelona - España<br/>
                    Tel. +34 93 395 13 78<br/>
                    oohhmusic@oohhmusic.com.es<br/>
                    <a href="mailto:nico_cris_y_alex@gmail.com@gmail.com"> nico_cris_y_alex@gmail.com </a>
                </p>
                <img id="Logo" src="logo_nico.png" alt=""/>
                <div id="Social">
                    <a href="https://es-es.facebook.com/" target="_blank">
                        <img id="Facebook" src="../dam1t1grupo2/img/logos redes sociales/Facebook-PNG-Image-38915.png" alt=""/>
                    </a>
                    <a href="https://www.instagram.com/?hl=es" target="_blank">
                        <img id="Instagram" src="../dam1t1grupo2/img/logos redes sociales/download-instagram-png-logo-20.png" alt=""/>
                    </a>
                    <a href="https://www.youtube.com/" target="_blank">
                        <img id="Youtube" src="../dam1t1grupo2/img/logos redes sociales/f2ea1ded4d037633f687ee389a571086-logotipo-de-youtube-icono-by-vexels.png" alt=""/>
                    </a>
                </div>
            </div>
        </section>
    </body>
    </html>
    <?php
} else {
    echo "<p style='color:red; text-align:center; font-size:xx-large'>Tienes que iniciar sesión</p>";
    header("Refresh:2; url=index.php", true, 303);
}
    
