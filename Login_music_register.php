<?php
if (isset($_POST["userlogin"])) {
    session_destroy();
} else {
    session_start();
    require_once 'bbdd.php';
    ?>
    <html class="musician">
        <head>
            <meta charset="UTF-8">
            <title>Pagina Login y Registro de Musicos</title>
            <link href="CSS/login_register.css" rel="stylesheet" type="text/css"/>
            <script src="JAVASCRIPT/pluguinJquery.js" type="text/javascript"></script>
            <script src="JAVASCRIPT/jquery.numeric.js" type="text/javascript"></script>
            <script src="JAVASCRIPT/JS_Perfiles.js" type="text/javascript"></script>
        </head>
        <body>
            <div id="main">
                <div><h1 id="tituloH1"> Musicians Register  </h1> </div>

                <!--        <div id="logoMusic1">
                            <img src="login_pics/774968_music_512x512.png" alt="musico1"/>
                        </div>-->
                <div>
                    <form id='register-form' action="" method='POST' enctype="multipart/form-data">
                        <input type="text" placeholder="Artistic name" name="nombreArtistico" required>
                        <input type="text" placeholder="Name" name="nombre" required>
                        <input type="text" placeholder="Surname" name="apellido"  required>
                        <input type="text" placeholder="Telephone number" name="telefono"  class="numeric" maxlength="9" required>
                        <input type="text" placeholder="E-mail" name="mail"  required>
                        <input type="text" placeholder="Web page" name="web" required>                   
                        <input type="text" placeholder="Group Components" name="componentes" required>

                        Genero:  <select name="genero" id="genero">
                            <?php
                            $generoArray = bbdd_get_genero();

                            foreach ($generoArray as $ArrayValues) {

                                echo "<option value ='" . $ArrayValues["genero"] . "'>";
                                echo $ArrayValues["genero"];
                                echo "</option>";
                            }
                            ?>
                        </select>

                        <br> <br>
                        <div>Selecciona Comunidad : <select name="cbx_comunidad" id="cbx_comunidad">
                            </select></div>
                        <div>Selecciona Provincia : <select name="cbx_provincia" id="cbx_provincia">
                            </select></div>
                        <div>Selecciona Municipio : <select name="cbx_municipio" id="cbx_municipio">
                            </select></div>

                        <input type="hidden" id="municipioTextoId" name="municipioId" value=""> 

                        <input type="password" placeholder="Password" name="pass" pattern=".{6,12}" required title="6 to 12 characters">
                        <input type="password" placeholder="Re Password" name="pass2" required>
                        <input type="file" name="fileupload" placeholder="Upload image" accept="image/*" required> 
                        <button type='submit' name="botonRegistro">Register</button>
                    </form>
                    <?php
                    if (isset($_POST["botonRegistro"])) {
                        require_once 'bbdd.php';
                        $user = $_POST["nombreArtistico"];
                        $pass = $_POST["pass"];
                        $genero = $_POST["genero"];
                        $nombreComponentes = $_POST["componentes"];
                        $nombre = $_POST["nombre"];
                        $apellido = $_POST["apellido"];
                        $telefono = $_POST["telefono"];
                        $mail = $_POST["mail"];
                        $web = $_POST["web"];
                        $id_ciudad = $_POST["municipioId"];
                        $pass2 = $_POST["pass2"];

                        $nombreImagen = basename($_FILES["fileupload"]["name"]);
                        if (comprobar_usuario_existe($user)) {

                            echo "<p style='color:blue;font-size:30px;background-color:red'>User already exists, try onother one..</p>";
                            return;
                        }

                        $passHash = password_hash($pass, PASSWORD_DEFAULT);

                        $conexion = conectar();
                        $usuario = mysqli_query($conexion, "SELECT * FROM musico WHERE nombre_artistico = '$user'");
                        desconectar($conexion);
                        if ($pass == $pass2) {
                            if (mysqli_num_rows($usuario) == 0) {
                                $resultado = bbdd_insertar_musico($user, $passHash, $genero, $nombreComponentes, $nombre, $apellido, $telefono, $id_ciudad, $nombreImagen, $mail, $web);
                                if ($resultado == "ok") {
                                    echo "<br>The user has been registered";
                                } else {
                                    echo "<br>$resultado";
                                }
                            } else {
                                echo "<br>Username is used. Try another one";
                            }
                        } else {
                            $mensajeErrorPass = "Passwords don´t match";
                            echo "<br>$mensajeErrorPass";
                        }

                        subirFoto($_FILES);
                    }
                    ?>
                </div>
                <!--        <div id="logoMusic2">
                            <img src="login_pics/774958_music_512x512.png" alt=""/>
                        </div>-->
                <form method="get" action="index.php">
                    <button id="myBtn" >Go Back</button> 
                </form>
            </div>
            <script src="JAVASCRIPT/JS_Register.js" type="text/javascript"></script>
            <script>
                iniciarSelects();
            </script>
        </body>
    </html>
    <?php
}
