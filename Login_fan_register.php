<?php
if (isset($_POST["userlogin"])) {
    session_destroy();
} else {
    session_start();
    require_once 'bbdd.php';
    ?>
    <html class="fan">
        <head>
            <meta charset="UTF-8">
            <title>Pagina Login y Registro de Fans</title>
            <script src="JAVASCRIPT/pluguinJquery.js" type="text/javascript"></script>
            <link href="CSS/login_register.css" rel="stylesheet" type="text/css"/>
            <script src="JAVASCRIPT/jquery.numeric.js" type="text/javascript"></script>
            <script src="JAVASCRIPT/JS_Perfiles.js" type="text/javascript"></script>
            
        </head>
        <body>
            <div id="main">
                <div><h1 id="tituloH1"> Fans Register  </h1> </div>

                <div>
                    <form id='register-form' name='combo' action="" method='POST' enctype="multipart/form-data">
                        <input type="text" placeholder="Username" name="username" required>
                        <input type="text" placeholder="Name" name="nombre" required>
                        <input type="text" placeholder="Surname" name="apellido" required>
                        <input type="text" placeholder="E-mail" name="email" required>
                        <input type="text" placeholder="Telephone number" name="telefono" class="numeric" maxlength="9" required>
                        <input type="text" placeholder="Direction" name="direccion" required> 

                        <div>Selecciona Comunidad : <select name="cbx_comunidad" id="cbx_comunidad">
                            </select></div>
                        <div>Selecciona Provincia : <select name="cbx_provincia" id="cbx_provincia">
                            </select></div>
                        <div>Selecciona Municipio : <select name="cbx_municipio" id="cbx_municipio">
                            </select></div>

                        <input type="hidden" id="municipioTextoId" name="municipioId"  value=""> 

                        <input type="password" placeholder="Password" name="pass" pattern=".{6,12}" required title="6 to 12 characters">
                        <input type="password" placeholder="Re Password" name="pass2" required>
                        <input type="file" name="fileupload" placeholder="Upload image" accept="image/*" required> 
                        <br />
                        <button type='submit' name="botonRegistro">Register</button>

                    </form>
                    <?php
                    if (isset($_POST["botonRegistro"])) {

                        $user = $_POST["username"];
                        $pass = $_POST["pass"];
                        $nombre = $_POST["nombre"];
                        $apellido = $_POST["apellido"];
                        $mail = $_POST["email"];
                        $telefono = $_POST["telefono"];
                        $direccion = $_POST["direccion"];
                        $id_ciudad = $_POST["municipioId"];
                        $pass2 = $_POST["pass2"];

                        $nombreImagen = basename($_FILES["fileupload"]["name"]);

//                        echo "<pre>";
//                        print_r($_FILES);
//                        echo "</pre>";

                        if (comprobar_usuario_existe($user)) {

                            echo "<p style='color:blue;font-size:30px;background-color:red'>User already exists, try onother one..</p>";
                            return;
                        }

                        $passHash = password_hash($pass, PASSWORD_DEFAULT);

                        $conexion = conectar();
                        $usuario = mysqli_query($conexion, "SELECT * FROM fan WHERE nombre_usuario_fan = '$user'");

                        desconectar($conexion);
                        if ($pass == $pass2) {

                            if (mysqli_num_rows($usuario) == 0) {
                                $resultado = bbdd_insertar_fan($user, $passHash, $nombre, $apellido, $mail, $telefono, $direccion, $id_ciudad, $nombreImagen);
                                if ($resultado == "ok") {
                                    echo "<br>The user has been registered";
                                } else {
                                    echo "<br>$resultado";
                                }
                            } else {
                                echo "<br>Username is used. Try another one";
                            }
                        } else {
                            $mensajeErrorPass = "Passwords don´t match";
                            echo "<br>$mensajeErrorPass";
                        }

                        subirFoto($_FILES);
                    }
                    ?>
                </div>
                <form method="get" action="index.php">
                    <button id="myBtn" >Go Back</button> 
                </form>
            </div>
            <script src="JAVASCRIPT/JS_Register.js" type="text/javascript"></script>
            <script>
                iniciarSelects();
            </script>
        </body>
    </html>
    <?php
}
