<?php
if (isset($_POST["userlogin"])) {
    session_destroy();
} else {
    session_start();
    require_once 'bbdd.php';
    ?>
    <html class="local">
        <head>
            <meta charset="UTF-8">
            <title>Pagina Login y Registro de Locales</title>
            <script src="JAVASCRIPT/pluguinJquery.js" type="text/javascript"></script>
            <link href="CSS/login_register.css" rel="stylesheet" type="text/css"/>
            <script src="JAVASCRIPT/jquery.numeric.js" type="text/javascript"></script>
            <script src="JAVASCRIPT/JS_Perfiles.js" type="text/javascript"></script>
        </head>
        <body>
            <div id="main">
                <div><h1 id="tituloH1"> Locals Register  </h1> </div>

                <div>
                    <form id='register-form' action="" method='POST' enctype="multipart/form-data">
                        <input type="text" placeholder="Name" name="nombre" required>
                        <input type="text" placeholder="Location" name="ubicacion" required>
                        <input type="text" placeholder="E-mail" name="mail"  required>
                        <input type="text" placeholder="Telephone number" name="telefono" class="numeric" maxlength="9" required>
                        <input type="text" placeholder="Maximum capacity" name="aforo" required> 

                        <div>Selecciona Comunidad : <select name="cbx_comunidad" id="cbx_comunidad">
                            </select></div>
                        <div>Selecciona Provincia : <select name="cbx_provincia" id="cbx_provincia">
                            </select></div>
                        <div>Selecciona Municipio : <select name="cbx_municipio" id="cbx_municipio">
                            </select></div>

                        <input type="hidden" id="municipioTextoId" name="municipioId" value=""> 

                        <input type="password" placeholder="Password" name="pass" pattern=".{6,12}" required title="6 to 12 characters">
                        <input type="password" placeholder="Re Password" name="pass2" required>
                        <input type="file" name="fileupload" placeholder="Upload image" accept="image/*" required> 

                        <button type='submit' name="botonRegistro">Register</button>
                    </form>
                    <?php
                    if (isset($_POST["botonRegistro"])) {
                        require_once 'bbdd.php';
                        $nombreLocal = $_POST["nombre"];
                        $pass = $_POST["pass"];
                        $ubicacion = $_POST["ubicacion"];
                        $mail = $_POST["mail"];
                        $telefono = $_POST["telefono"];
                        $aforo = $_POST["aforo"];
                        $id_ciudad = $_POST["municipioId"];
                        $pass2 = $_POST["pass2"];

                        $nombreImagen = basename($_FILES["fileupload"]["name"]);

                        if (comprobar_usuario_existe($nombreLocal)) {

                            echo "<p style='color:black;font-size:30px;background-color:red'>User already exists, try onother one..</p>";
                            return;
                        }

                        $passHash = password_hash($pass, PASSWORD_DEFAULT);

                        $conexion = conectar();
                        $usuario = mysqli_query($conexion, "SELECT * FROM locales WHERE nombre_local = '$nombreLocal'");
                        desconectar($conexion);
                        if ($pass == $pass2) {
                            if (mysqli_num_rows($usuario) == 0) {
                                $resultado = bbdd_insertar_local($nombreLocal, $passHash, $ubicacion, $id_ciudad, $mail, $telefono, $aforo, $nombreImagen);
                                if ($resultado == "ok") {

                                    echo "<br>The user has been registered";
                                } else {
                                    echo "<br>$resultado";
                                }
                            } else {
                                echo "<br><p style='color:black;font-size:30px;background-color:red'>Username is used. Try another one</p>";
                            }
                        } else {
                            $mensajeErrorPass = "<p style='color:black;font-size:30px;background-color:red'>Passwords don´t match</p>";
                            echo "<br>$mensajeErrorPass";
                        }

                        subirFoto($_FILES);
                    }
                    ?>

                </div>
                <!--            <div id="logoMusic2">
                                <img src="login_pics/ticket-256.png" alt=""/>
                            </div>-->

                <form method="get" action="index.php">
                    <button id="myBtn" >Go Back</button> 
                </form>
            </div>
            <script src="JAVASCRIPT/JS_Register.js" type="text/javascript"></script>
            <script>
                iniciarSelects();
            </script>
        </body>
    </html>
    <?php
}
