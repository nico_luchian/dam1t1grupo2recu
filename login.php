<?php
session_start();
require_once 'bbdd.php';
?>
<html class="login">
    <head>
        <meta charset="UTF-8">
        <title>Pagina Login y Registro de Fans</title>
        <script src="jquery-3.2.1.min.js" type="text/javascript"></script>
        <link href="CSS/login_register.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div id="main" class="login">
            <div><h1 id="tituloH1" class="login"> LOGIN  </h1> </div>
            <form id='login-form' action="" method='POST' class="login">
                <input type="text" placeholder="Username" name="userlogin_fan" value="" required class="login">
                <input type="password" placeholder="Password" name="userpass_fan" value="" required class="login">
                <button type='submit' name="login_user" class="button">Login</button>
            </form>
            <?php
            echo "<br>";
            if (isset($_POST["login_user"])) {

                $nombre = $_POST["userlogin_fan"];
                $pass = $_POST["userpass_fan"];

                $result = comprobar_usuario("nombre_usuario_fan", "password_fan", "fan", $nombre, $pass);

                if ($result) {

                    header("Location: Perfil_Fan.php");
                    return;
                }

                $result = comprobar_usuario("nombre_local", "password_local", "locales", $nombre, $pass);

                if ($result) {
                    header("Location: Perfil_Local.php");
                    return;
                }

                $result = comprobar_usuario("nombre_artistico", "password_musico", "musico", $nombre, $pass);

                if ($result) {
                    header("Location: Perfil_Musico.php");
                } elseif (!$result) {
                    echo "<p style='color:white;font-size:30px;'>Incorrect username or password</p>";
                }
            }
            ?>
        </div>
        <form method="get" action="index.php">
            <button id="myBtn" >Go Back</button> 
        </form>
    </body>
</html>


