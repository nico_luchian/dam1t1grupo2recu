<?php
if (!isset($_SESSION)) {
    session_start();
}
?>
<html>
    <head>
        <title>Musicos Transversal</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="JAVASCRIPT/pluguinJquery.js" type="text/javascript"></script>
        <script src="plugin/dist/jquery.validate.js" type="text/javascript"></script>
        <script src="plugin/dist/additional-methods.js" type="text/javascript"></script>

        <link href="plugin/slick/slick-theme.css" rel="stylesheet" type="text/css"/>
        <link href="plugin/slick/slick.css" rel="stylesheet" type="text/css"/>
        <script src="plugin/slick/slick.min.js" type="text/javascript"></script>
        <script src="JAVASCRIPT/JAVASCRIPT_PROYECTO.js" type="text/javascript"></script>
        <link href="CSS/Index_css.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div id="menu">

            <ul>
                <li><button id="button1">Ver Locales</button></li>
                <li><button id="button2">Ver Artistas</button></li>
                <li><button id="button3">Votación</button></li>
                <li><button onclick="window.location.href = 'galeria_musicos.html'" id="button4" >Sobre nosotros</button></li>
            </ul>

        </div>

        <div id="main">

            <!--------------------------------------------------HEADER-------------------------------------------------->
            <div id="divlogo">
                <img id="logopng" src="img/logo_nico.png" alt="logo_transversal"/>
            </div>




            <!--            <div id="buscar">
                            <input type="search" id="buscador" placeholder="">
                            <p>Buscador</p>
            
                        </div>-->


            <div id="proximoconcierto">

                <p id="concierto"> Conoce a Tash Sultana, es una música australiana de 21 años que sabe tocar 10 instrumentos distintos: trompeta, guitarra, hace beatbox… </p>

            </div>
            <span id="proximoconciertospan">El video de la semana:</span>

            <!-------------------------------------------LOGIN------------------------------------------------------->

            <div id="login">

                <span>¿Todavía sin cuenta?</span>
                <br>
                <a href="login.php"><button>Login</button></a>
                <button id="login2">Crear tu cuenta</button>

            </div>


            <div id="login3">


                <a href="../transversal_recu/Login_fan_register.php">FAN</a>
                <br>
                <a href="../transversal_recu/Login_music_register.php">MÚSICO</a>
                <br>
                <a href="../transversal_recu/Login_local_register.php">LOCAL</a>




            </div>




            <!------------------------------------------------BODY--------------------------------------------->


            <div id="divmusicos">
                <img id="musicos" src="img/dead_musicians_together.jpg" alt=""/>

            </div>


            <div id="titulo">
                <h1>OohMusic</h1>
                <h3>Apoya a tus artistas favoritos con la ayuda de nuestra plataforma. 
                    Aquí los músicos y los fans podrán estar más cerca que nunca. </h3> 
            </div>
            <div id="video">
                <iframe width="350" height="200" src="https://www.youtube.com/embed/LWGKtVFjar0?start=158" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>


            <div class="article" >

                <button class="accordion">David Sainz:'Quiero llegar a ser num.1 en discos</button>
                <div class="panel">
                    <p> Nuestro ilustre músico David Sainz nos confiesa que le encantaría seguir creciendo. Comenzó aquí haciendo sus primeros conciertos y dándose a conocer, ahora está muy agradecido y sigue subiendo..</p>
                </div>

                <button class="accordion">Aforo maximo otra vez en Bolshio...</button>
                <div class="panel">
                    <p >Lo volvieron a hacer...ya es el quinto concierto consecutivo que el Local 'Bolshio' vuelve a llenar el aforo. ¡¡Muchas felicidades de nuevo!!</p>
                </div>
                <button class="accordion">El ganador del concurso...</button>
                <div class="panel">
                    <p >El ganador del concurso de la semana pasada,(Rodrigo Pérez) acertó  nada más y nada menos que la puntuación exacta del top 3 del ranking de músicos!!!Guau!!!</p>
                </div>

            </div>
            <div id="coment"><p>MURO</p></div>
            <div id="chat"> 




            </div>

            <div id="chat1">

                <textarea rows="4" cols="50" id="textochat" name="textochat" placeholder="Introduce tu texto aquí" >
                </textarea>

                <input  type="button"  id="botonchat" name="botonchat" value="COMENTA">

            </div>
            <div id="video2">
                <iframe width="300" height="200" src="https://www.youtube.com/embed/wFclmhF2kj4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>








            <div id="contenedor1">
                <div id="tituloentrevista1">
                    <p>SABIAS QUE...</p>
                </div>


                <div id="entrevista1">

                    <div>
                        <img id='imagenentrevista1_1'src="img/stringuers.png" alt=""/>
                        <p id="p1"> -THE STRINGUERS- Ya han publicado el vídeo oficial  para "Tengomiedo.com", tema extraído de su último álbum, "Dimelo con Strings", disponible desde el pasado mayo.</p>
                    </div>

                    <div>
                        <img id='imagenentrevista1_2' src="img/freddy.jpg" alt=""/>
                        <p id="p2" >Freddie Mercuri, antes de ser homosexual, estuvo al lado de Mary Austin, mujer con la cual estuvo 6 años en pareja!!!! Cuando a Freddie le detectan Sida, es a esta mujer, la primer persona a quién se lo confiesa. De ahí en más, ella estuvo inseparablemente al lado de él. Cuando Freddie pierde la vista, cuando Freddie no puede levantarse de la cama, estaba ella ahí para ayudarlo. Freddie en agradecimiento, en el testamento le deja absolutamente toda su fortuna a Mary. Su mansion, sus muebles, su dinero, su todo...Sólo Mary sabe donde fueron colocadas las cenizas de Freddie Mercury. Le dio a ella la responsabilidad de encargarse de las mismas y le hizo prometer que nunca diría a nadie dónde fueron esparcidas. No sólo fue un músico con todas las letras. Fue un caballero, un hombre único y admirable </p>                 
                    </div>

                    <div>
                        <img id='imagenentrevista1_3' src="img/slash.jpg" alt=""/>
                        <p> Saul Hudson más conocido por su nombre artístico Slash, es un guitarrista y compositor británico-estadounidense.​ Es el guitarrista principal del grupo de rock estadounidense Guns N' Roses.</p>
                    </div>
                    <div>
                        <img id='imagenentrevista1_4' src="img/heroes.jpg" alt=""/>
                        <p>Kurt Cobain y Axel Rose, no se podían ni ver, es más cuando ambos se cruzaban en ciertos festivales, Kurt se burlaba de el y Axel lo puteaba detras de sus guarespaldas...Pero eso no es todo señores!!!! Kurt junto a Novoselic, se encargaba en los recitales, entre tema y tema ocacionalmente para reirse y burlarse de Axel con su público. Rose no se quedaba atras, hasta se burlo de Cobain una vez muerto...si buscan en Youtube podrán encontrar algunos videos... </p>
                    </div>


                </div>
            </div>










            <div id="titulomasvotados">
                <p>Musicos mas votados</p>

            </div>  
            <div id="tituloproxconciertos">
                <p>Proximos conciertos</p>

            </div>


            <div id="contenedor2">
                <div id = "tablamusicosvotados">

                    <?php
                    require_once './bbdd_perfiles.php';
                    echo"<table class='sample' style='width:100%';>";
                    echo "<tr><th>Nombre musico</th><th>Genero</th><th>Votos</th>";
                    $musicosArray = losMusicosMasVotados();
                    while ($fila = mysqli_fetch_array($musicosArray)) {
                        extract($fila);
                        echo "<tr style='color:#AFA;text-align:center;'><td>$nombre_artistico</td><td>$genero</td><td>$votos</td>";
                    }
                    echo'</table>';
                    ?>

                </div>
            </div>
            <div id="contenedor2_1">
                <div id = "tablaproxconcierto">

                    <?php
                    require_once './bbdd_perfiles.php';

                    $total = totalConciertos();
                    $filasPorPagina = 6;

                    if (isset($_GET["contador"])) {
                        $contador = $_GET["contador"];
                    } else {
                        $contador = 0;
                    }

                    echo"<table class='sample' style='width:100%';>";
                    echo "<tr><th>Nombre Concierto</th><th>Local</th><th>Artista</th><th>Local</th><th>Fecha</th><th>Genero</th>";

                    $platos = selectConciertosDesde($contador, $filasPorPagina);
                    while ($fila = mysqli_fetch_array($platos)) {
                        extract($fila);
                        echo "<tr style='color:#AFA;text-align:center;'><td>$nombre_concierto</td><td>$nombre_local</td><td>$nombre_artistico</td><td>$nombre_local</td><td>$fecha_concierto</td><td>$genero</td>";
                    }
                    echo'</table>';
                    echo "<div style='color:white' >";
                    if ($contador > 0) {
                        echo "<a id='boton_atras' href='index.php?contador=" . ($contador - $filasPorPagina) . "'>Anterior </a>";
                    }

                    if (($contador + $filasPorPagina) <= $total) {
                        echo "<p style='color:#AFA;text-align:center;'>Mostrando de " . ($contador + 1) . " a " . ($contador + $filasPorPagina) . " de $total </p>";
                    } else {
                        echo "<p style='color:#AFA;text-align:center;'Mostrando de " . ($contador + 1) . " a $total de $total</p>";
                    }
                    if (($contador + $filasPorPagina) < $total) {
                        echo "<a id='boton_siguente' href='index.php?contador=" . ($contador + $filasPorPagina) . "'> Siguiente</a>";
                    }
                    echo " </div>";
                    ?>


                </div>
            </div>










            <div id = "contenedor3">
                <br>
                <div id = "titulotracks">
                    <p id = "titulotracksP">Música</p>
                </div>


                <div id = "tracks">

                    <div id="primerafila">
                        <ul>
                            <li>
                                <div><p class = "musicadescargada">Stir It Up. Bob Marley & The Wailers<p></div>
                                <audio class = "audio" src = "MUSICA/Bob Marley  The Wailers - Stir It Up.mp3" preload = "none" controls></audio>
                            </li>

                            <li>
                                <div><p class = "musicadescargada">Fantastic Shine. Love of Lesbian<p></div>
                                <audio class = "audio"src = "MUSICA/Love of Lesbian - Fan.mp3" type = "audio/mpeg" preload = "none" controls>
                            </li>
                            <li>
                                <div><p class = "musicadescargada">Amsterdam.Broken Machine(Deluxe)<p></div>
                                <audio class = "audio"src = "MUSICA/Nothing But Thieves - Amsterdam (Official Video).mp3" preload = "none" controls></audio>

                            </li>

                            <li> <div><p class = "musicadescargada">Don't Mind. Kent Jones<p></div>
                                <audio  class="audio"src="MUSICA/Kent Jones - Dont Mind.mp3" preload="none" controls></audio>

                            </li>
                            <li>     <div><p class="musicadescargada">Kites. Anik Khan<p></div>
                                <audio  class="audio"src="MUSICA/Kites - Anik Khan (prod. Jarreau Vandal Raj Makhija  Anik Khan).mp3" preload="none" controls></audio>
                            </li>


                        </ul>
                    </div>
                    <div id="segundafila">
                        <ul>
                            <li>
                                <div><p class="musicadescargada">Locked Out Of Heaven. Bruno Mars<p></div>
                                <audio  class="audio" src="MUSICA/Bruno Mars - Locked Out Of Heaven [OFFICIAL VIDEO].mp3" preload="none" controls></audio>

                            </li>
                            <li>
                                <div><p class="musicadescargada">Welcome to Jamrock. Damian Marley<p></div>
                                <audio  class="audio"src="MUSICA/Damian Marley - Welcome To Jamrock.mp3" preload="none" controls></audio></li>
                            <li>
                                <div><p class="musicadescargada">Radioactive. Imagine Dragons<p></div>
                                <audio  class="audio"src="MUSICA/Radioactive-Imagine Dragons (Lyrics).mp3" preload="none" controls></audio></li>

                            <li>     <div><p class="musicadescargada">I think I Like It. Fake Blood<p></div>
                                <audio  class="audio"src="MUSICA/Fake Blood - I Think I Like It.mp3" preload="none" controls></audio></li>

                            <li>     <div><p class="musicadescargada">Blurryface. Twenty one pilots<p></div>
                                <audio  class="audio"src="MUSICA/Twenty One Pilots - Ride (Blurryface).mp3" preload="none" controls></audio></li>

                        </ul>
                    </div>
                </div>

            </div>



            <div id="footer">

                <div>
                    <span>c/Sant Quirze, 46-49, 2º 1º A
                        <br>

                        08005 - Barcelona - España
                        <br>

                        Tel. +34 93 395 13 78
                        <br>
                        <a href="mailto:nico_cris_y_alex@gmail.com@gmail.com">Enviar_email</a></span>
                    <br>

                </div>

                <div id="contenedorseguirnos">
                    <small >Síguenos:</small>
                    <a href= "https://www.instagram.com/stucom/?hl=es">      
                        <img src="img/logos redes sociales/download-instagram-png-logo-20.png" alt=""/>
                    </a>
                    <a href="https://www.facebook.com/stucombarcelona/"> 
                        <img src="img/logos redes sociales/Facebook-PNG-Image-38915.png" alt=""/>
                    </a>
                    <a href="https://www.youtube.com/channel/UCMlm3vx3NKsW1Rs_m_3rnbg">
                        <img src="img/logos redes sociales/f2ea1ded4d037633f687ee389a571086-logotipo-de-youtube-icono-by-vexels.png" alt=""/>
                    </a>
                </div>






            </div>


            <div id="listado">
                <h3 style="color:white">Listado de conciertos</h3>
                <div class="closelistado" >
                    <img  src="img/x.png" alt=""/>
                </div>

                <div class="lista">
                    <?php
                    require_once 'bbdd_perfiles.php';
                    $todoslosconciertos = select_de_todos_los_locales();
                    echo "<table>";
                    echo "<tr>";
                    echo "<th style='background-color:black'>Nombre</th><th style='background-color:black'>Ubicación</th><th style='background-color:black'>Municipio</th><th style='background-color:black'>Telefono</th>";
                    echo "</tr>";

                    while ($fila = mysqli_fetch_assoc($todoslosconciertos)) {

                        echo "<tr>";

                        foreach ($fila as $dato) {

                            echo "<td style='background-color:white'>$dato</td>";
                        }
                        echo "</tr>";
                    }
                    echo "</table>";
                    ?>
                </div>

            </div>





            <div id="listado2" >
                <h3 style="color:white">Artistas</h3>
                <div class="closelistado" >

                    <img  src="img/x.png" alt=""/>
                </div>

                <div class="lista">
                    <?php
                    $todoslosArtistas = select_de_todos_los_artistas();
                    echo "<table>";
                    echo "<tr>";
                    echo "<th style='background-color:black'>Artista</th><th style='background-color:black'>Genero</th><th style='background-color:black'>Nombre</th><th style='background-color:black'>Apellido</th><th style='background-color:black'>Web</th>";
                    echo "</tr>";

                    while ($fila = mysqli_fetch_assoc($todoslosArtistas)) {

                        echo "<tr>";

                        foreach ($fila as $dato) {

                            echo "<td style='background-color:white'>$dato</td>";
                        }
                        echo "</tr>";
                    }
                    echo "</table>";
                    ?>

                </div>

            </div>




            <div id="listado3">
                <h3 style="color:white">Votos de conciertos</h3>
                <div class="closelistado" >
                    <img  src="img/x.png" alt=""/>
                </div>
                <div  id="listapuntosyvotos1">
                    <?php
                    $todoslosConciertos_votados = select_de_votos_Conciertos();
                    echo "<table>";
                    echo "<tr>";
                    echo "<th style='background-color:black'>Concierto</th><th style='background-color:black'>Votos</th>";
                    echo "</tr>";
                    while ($fila = mysqli_fetch_assoc($todoslosConciertos_votados)) {
                        echo "<tr>";
                        foreach ($fila as $dato) {
                            echo "<td  style='background-color:white'>$dato</td>";
                        }
                        echo "</tr>";
                    }
                    echo "</table>";
                    ?>
                </div>

                <div id="listapuntosyvotos2">
                    <?php
                    $todoslosMusicos_votados = select_de_votos_Musicos();
                    echo "<table>";
                    echo "<tr>";
                    echo "<th style='background-color:black';>Musicos</th><th style='background-color:black'>Puntos</th>";
                    echo "</tr>";
                    while ($fila = mysqli_fetch_assoc($todoslosMusicos_votados)) {
                        foreach ($fila as $dato) {
                            echo "<td  style='background-color:white'>$dato</td>";
                        }
                        echo "</tr>";
                    }
                    echo "</table>";
                    ?>
                </div>
            </div>          
        </div>
        <img style="width: 4%" id="myBtn" src="img/go_up_buton.png" alt=""/>

        <div id="publicidad0">
            <img src="img/moto.jpg" alt="tiesto"/>
        </div>
        <div id="publicidad1">
            <img  src="img/burguer.jpg" alt="tiesto"/>
        </div>
        <div id="publicidad1_1">
            <img  src="img/50img.jpg" alt="tiesto"/>
        </div>

        <div id="publicidad2">
            <img src="img/cocacola.jpg" alt=""/>
        </div>
        <div id="publicidad2_1">
            <img src="img/cocacola.jpg" alt=""/>
        </div>
        <div id="publicidad3">
            <img src="img/coche.jpg" alt=""/>

        </div>
        <div id="publicidad4">
            <img src="img/marshall.jpg" alt=""/>
        </div>
        <div id="publicidad5">
            <img src="img/viaje.jpg" alt=""/>
        </div>
        <div id="publicidad6">
            <img src="img/ordenador.jpg" alt=""/>
        </div>
        <div id="publicidad7">
            <img src="img/guitarsold.jpg" alt=""/>
        </div>
        <div id="publicidad8">
            <img src="img/perfume.jpg" alt=""/>
        </div>
        <div id="publicidad9">
            <img src="img/esqui.jpg" alt=""/>
        </div>

    </body>
</html>
