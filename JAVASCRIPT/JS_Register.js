function iniciarSelects() {

    cargarComunidades();
}

$("#cbx_comunidad").change(function () {

    cargarProvincias();
});

$("#cbx_provincia").change(function () {

    cargarMunicipios();
});

$("#cbx_municipio").change(function () {

    obtenerIdMunicipio();
});

function obtenerIdMunicipio() {
    var municipioNombre = $("#cbx_municipio").val();
    var params = {action: "Query", query: `select id_ciudad from ciudad where municipio='${municipioNombre}'`};

    callAjaxBBDD(params, function (result) {

        var id = result[0]["id_ciudad"];
        $("#municipioTextoId").val(id);
    });
}

function cargarComunidades() {
    var params = {action: "Query", query: "select comunidad from ciudad group by comunidad"};

    callAjaxBBDD(params, function (result) {

        $("#cbx_comunidad").empty();
        $.each(result, function (i, item) {

            $("#cbx_comunidad").append("<option>" + item.comunidad + "</option>");
        });

        cargarProvincias();
    });
}

function cargarProvincias()
{
    var comunidad = $("#cbx_comunidad").val();
    var params = {action: "Query", query: `select provincia from ciudad where comunidad='${comunidad}' group by provincia`};

    callAjaxBBDD(params, function (result) {

        $("#cbx_provincia").empty();
        $.each(result, function (i, item) {

            $("#cbx_provincia").append("<option>" + item.provincia + "</option>");
        });

        cargarMunicipios();
    });
}

function cargarMunicipios()
{
    var provincia = $("#cbx_provincia").val();
    var params = {action: "Query", query: `select municipio from ciudad where provincia='${provincia}'`};

    callAjaxBBDD(params, function (result) {

        $("#cbx_municipio").empty();
        $.each(result, function (i, item) {

            var option = $("<option>" + item.municipio + "</option>");
            $("#cbx_municipio").append(option);
        });

        obtenerIdMunicipio();
    });
}

function callAjaxBBDD(params, func) {

    $.ajax({

        type: "POST",
        url: "bbdd.php",
        dataType: "json",
        data: params,
        cache: false,
        success: function (respuestaJson) {
            func(respuestaJson);
        },
        error: function (respuestaJson) {
            func(respuestaJson);
        }
    });
}

