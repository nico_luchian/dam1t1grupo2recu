$(document).ready(inici);
function inici() {

    $("#changeLocalData").validate({
        focusCleanup: true,
        rules: {
            perfUbicacion: {
                required: true,
                minlength: 3

            },
            perfEmail: {
                required: true

            },
            perfTelefono: {
                required: true
            },
            perfAforo: {
                required: true

            }
        }, messages: {
            perfUbicacion: {
                required: "<b>required</b>",
                minlength: "Please, at least 3 characters"

            },
            perfEmail: {
                required: "<b>required</b>"
            },
            perfTelefono: {
                required: "<b>required</b>"
            },
            perfAforo: {
                required: "<b>required</b>"
            }
        }
    });
    $("#changeLocalData").valid();

    $("#concertValidator").validate({
        focusCleanup: true,
        rules: {
            nombreConcierto: {
                required: true,
                minlength: 3

            },
            FechaConcierto: {
                required: true

            },
            hora: {
                required: true
            },
            PrecioEntrada: {
                required: true

            },
            PropuestaEconomica: {
                required: true

            }
        }, messages: {
            nombreConcierto: {
                required: "<b>required</b>",
                minlength: "Please, at least 3 characters"

            },
            FechaConcierto: {
                required: "<b>required</b>"
            },
            hora: {
                required: "<b>required</b>"
            },
            PrecioEntrada: {
                required: "<b>required</b>"
            },
            PropuestaEconomica: {
                required: "<b>required</b>"
            }
        }
    });
    $("#concertValidator").valid();

    $("#changeUbi").validate({
        focusCleanup: true,
        rules: {
            newUbicacion: {
                required: true,
                minlength: 3

            }
        }, messages: {
            newUbicacion: {
                required: "<b>required</b>",
                minlength: "Please, at least 3 characters"

            }
        }
    });
    $("#changeUbi").valid();


    $("#chageEmail").validate({
        focusCleanup: true,
        rules: {
            newEmail: {
                required: true


            }
        }, messages: {
            newEmail: {
                required: "<b>required</b>"

            }
        }
    });
    $("#chageEmail").valid();
//////////////////////////////////////////////////////////////////




    $("#changeTel").validate({
        focusCleanup: true,
        rules: {
            newTelefono: {
                required: true


            }
        }, messages: {
            newTelefono: {
                required: "<b>required</b>"

            }
        }
    });
    $("#changeTel").valid();


}