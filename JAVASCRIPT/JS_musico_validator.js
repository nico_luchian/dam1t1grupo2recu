$(document).ready(inici);
function inici() {


    $("#changeDatosMusico").validate({
        focusCleanup: true,
        rules: {
            perfNombre: {
                required: true,
                minlength: 3

            },
            perfApellidos: {
                required: true

            },
            perfComponentes: {
                required: true
            },
            perfEmail: {
                required: true

            },
            perfTelefono: {
                required: true,
                minlength: 9
            },
            perfWeb: {
                required: true
            }
        }, messages: {
            perfNombre: {
                required: "<b>required</b>",
                minlength: "Please, at least 3 characters"

            },
            perfApellidos: {
                required: "<b>required</b>"
            },
            perfComponentes: {
                required: "<b>required</b>"
            },
            perfEmail: {
                required: "<b>required</b>"
            },
            perfTelefono: {
                required: "<b>required</b>"
            },
            perfWeb: {
                required: "<b>required</b>"
            }
        }
    });
    $("#changeDatosMusico").valid();

    $("#changeComponentesMusico").validate({
        focusCleanup: true,
        rules: {
            newComponentes: {
                required: true,
                range: [0, 10]


            }
        }, messages: {
            newComponentes: {
                required: "<b>required</b>"

            }
        }
    });
    $("#changeComponentesMusico").valid();


    $("#chageNombremusico").validate({
        focusCleanup: true,
        rules: {
            newNombre: {
                required: true,
                minlength: 3

            }
        }, messages: {
            newNombre: {
                required: "<b>required</b>",
                minlength: "Please, at least 3 characters"

            }
        }
    });
    $("#chageNombremusico").valid();



    $("#changeApeliidoMusico").validate({
        focusCleanup: true,
        rules: {
            newApellido: {
                required: true,
                minlength: 3

            }
        }, messages: {
            newApellido: {
                required: "<b>required</b>",
                minlength: "Please, at least 3 characters"

            }
        }
    });
    $("#changeApeliidoMusico").valid();



    $("#chageContactoMusico").validate({
        focusCleanup: true,
        rules: {
            newEmail: {
                required: true


            }
        }, messages: {
            newEmail: {
                required: "<b>required</b>"


            }
        }
    });
    $("#chageContactoMusico").valid();



    $("#changeTelefonoMusico").validate({
        focusCleanup: true,
        rules: {
            newTelefono: {
                required: true,
                range: [9, 9]


            }
        }, messages: {
            newTelefono: {
                required: "<b>required</b>"

            }
        }
    });
    $("#changeTelefonoMusico").valid();



    $("#changeWebMusico").validate({
        focusCleanup: true,
        rules: {
            newWeb: {
                required: true


            }
        }, messages: {
            newWeb: {
                required: "<b>required</b>"


            }
        }
    });
    $("#changeWebMusico").valid();
}